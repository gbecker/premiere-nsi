Activité d'introduction : attributs `class` et `id`
===================================================

# Préambule

L'objectif de cette activité d'introduction est de comprendre le rôle des attributs `class` et `id` des éléments HTML. Ces deux attributs sont extrêmement utilisés pour styliser des éléments avec le langage CSS ou pour gérer l'interactivité avec le langage JavaScript (dont vous aurez un premier aperçu cette année en NSI).

Dans cette activité nous nous intéresserons à l'utilisation de ces attributs pour styliser en CSS certains éléments d'une page Web.

<div class="zone-telechargement impression">
    <h1 class="titre-zone-telechargement not_before">Fichier à télécharger</h1>
    <p>Voici le fichier compressé à télécharger qui contient les fichiers HTML, CSS (et JS) nécessaires à l'activité : <a href="data/attributsClassId.zip" target="_blank" download="attributsClassId.zip">attributsClassId.zip</a></p>
</div>

La page `index.html` de cette archive contient le début de code suivant 

```html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width">
        <title>Activité d'introduction</title>
        <link href="style.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <h1>Les attributs <code>class</code> et <code>id</code></h1>
        
        <h2>L'attribut <code>class</code></h2>
        
        <p><strong>Il permet de cibler <em>plusieurs</em> balises</strong> pour leur appliquer un style. En CSS, on fera précéder le nom de l'attribut d'un point (.).</p>
        
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aut neque, similique quo molestias velit perspiciatis pariatur, dolorem a quisquam fugit vero voluptatum architecto consequatur adipisci obcaecati placeat natus debitis corrupti!</p>
        <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Perspiciatis dignissimos commodi corporis eius aut veniam dolore debitis libero deleniti natus voluptates sit inventore suscipit in quos distinctio vero, sunt adipisci?</p>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Maiores optio quod quis ratione cum dolorum aut perferendis exercitationem. Minima pariatur, quibusdam laborum excepturi similique tempore eligendi autem a dolorem maxime?</p>
        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ipsa accusamus aspernatur ex doloribus iusto accusantium fugit possimus blanditiis minima doloremque. Blanditiis laudantium sed reiciendis. Nesciunt ab accusantium exercitationem nulla obcaecati.</p>
            
        [...]
    </body>
</html>
```

La page s'affiche de cette manière dans le navigateur :

<img class="centre image-responsive image-encadree" src="data/act_intro_page_web.png" alt="capture d'écran" width="700">

## Intérêt des attributs `class` et `id`

Si on écrit le code CSS suivant dans le fichier `style.css` (associé à notre fichier HTML)

```css
p {
    color: blue;
}
```

alors **tous** les paragraphes de la page seront affichés en bleu :

<img class="centre image-responsive image-encadree" src="data/act_intro_page_web_2.png" alt="capture d'écran" width="700">

Les attributs `class` et `id` vont permettre de cibler uniquement certains éléments d'une page pour les styliser ou les manipuler : par exemple, on va pouvoir cibler un ou plusieurs paragraphes précis pour leur appliquer un style, qui ne s'appliquera pas aux autres.


# L'attribut `class`

<div class="important">
    <p>L'attribut <code>class</code> permet de cibler <em>plusieurs</em> éléments HTML. Les classes permettent de manipuler les éléments via CSS ou JavaScript.</p>
</div>



**Exemple** : On aimerait que les deux premiers paragraphes en latin (uniquement ceux-là) soient colorés en bleu.

Pour cela, on commence par définir une classe pour ces deux premiers paragraphes :

```html
<p><strong>Il permet de cibler <em>plusieurs</em> balises</strong> pour leur appliquer un style. En CSS, on fera précéder le nom de l'attribut d'un point (.).</p>
<p class="para-bleu">Lorem ipsum dolor sit amet consectetur adipisicing elit. Aut neque, similique quo molestias velit perspiciatis pariatur, dolorem a quisquam fugit vero voluptatum architecto consequatur adipisci obcaecati placeat natus debitis corrupti!</p>
<p class="para-bleu">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Perspiciatis dignissimos commodi corporis eius aut veniam dolore debitis libero deleniti natus voluptates sit inventore suscipit in quos distinctio vero, sunt adipisci?</p>
<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Maiores optio quod quis ratione cum dolorum aut perferendis exercitationem. Minima pariatur, quibusdam laborum excepturi similique tempore eligendi autem a dolorem maxime?</p>
<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ipsa accusamus aspernatur ex doloribus iusto accusantium fugit possimus blanditiis minima doloremque. Blanditiis laudantium sed reiciendis. Nesciunt ab accusantium exercitationem nulla obcaecati.</p>
```

On peut alors appliquer un style CSS à cette classe. Il faut faire précéder le nom de la classe par un `.` en CSS :

```css
.para-bleu {
    color: blue;
} 
```

On obtient alors la page suivante :

<img class="centre image-responsive image-encadree" src="data/act_intro_page_web_3.png" alt="capture d'écran" width="700">


# L'attribut `id`

<div class="important">
    <p>L'attribut <code>id</code> permet de cibler <em>un</em> élément HTML. Il s'agit d'un identifiant qui doit être <em>unique</em> pour l'ensemble du document. Il permet d'identifier un élément que l'on souhaite mettre en forme avec CSS ou manipuler avec un script.</p>
</div>



**Exemple** : On aimerait que le premier titre de niveau `h1` soit centré (et pas l'autre).

Pour cela, on commence par définir un attribuer un identifiant à ce titre :

```html
<h1 id="titre-principal">Les attributs <code>class</code> et <code>id</code></h1>
```

On peut alors appliquer un style à ce titre grâce à son attribut `id`. Il faut faire précéder le nom de l'identifiant par un `#` en CSS :

```css
#titre-principal {
    text-align: center;
}
```

On obtient alors la page suivante :

<img class="centre image-responsive image-encadree" src="data/act_intro_page_web_4.png" alt="capture d'écran" width="700">

<div class="a-faire titre" markdown="1">

### ✏️ Défis : à vous de jouer !

**Défi 1** : Les titres de niveau 2 "L'attribut class" et "L'attribut id" doivent être centrés et en bleu (mais pas ceux des défis !)

**Défi 2** : Les 4 paragraphes précédant la première image doivent être écrits en rouge.

**Défi 3** : Le deuxième titre de niveau 1 doit avoir une taille de police égale à 50px (mais pas l'autre !).

**Défi 4** : Le dernier paragraphe de chaque partie (celui avant l'image) doit être aligné à droite.

**Défi 5** : Seul le premier titre de niveau 2 (à savoir "L'attribut class") doit être écrit en blanc sur fond noir au survol de la souris. On pourra chercher de la documentation sur le pseudo-format :hover.

**Défi 6** : La deuxième image uniquement doit être centrée. Vous pourrez chercher de la documentation sur le Web.

</div>

# Pour aller plus loin si nécessaire

> Cette section est intéressante pour celles et ceux qui souhaitent approfondir les langages HTML et CSS, mais aussi dans le cadre de projets Web qui nécessiteraient davantage de connaissances.

Dès que l'on veut écrire des pages un tant soit peu plus complètes et jolies, il est important de maîtriser les balises universelles `<div>` et `<span>`, mais aussi la mise en page avec **Flexbox** (et **Grid Layout**), ainsi que la sélection des éléments.

## Les balises universelles `<div>` et `<span>`

Voici une vidéo très bien faite qui présente ces deux éléments :

<div class="video-responsive">
    <div class="youtube_player centre" videoID="_QJx7gOt5iU" width="560" height="315" theme="light" rel="1" controls="1" showinfo="1" autoplay="0"></div>
</div>
<p class="impression">
    Source : <a href="https://youtu.be/_QJx7gOt5iU">https://youtu.be/_QJx7gOt5iU</a>
</p>

>Vous trouverez également tout ce qu'il faut savoir sur *MDN Web docs* : [https://developer.mozilla.org/fr/docs/Web/HTML/Element/div](https://developer.mozilla.org/fr/docs/Web/HTML/Element/div) et [https://developer.mozilla.org/fr/docs/Web/HTML/Element/span](https://developer.mozilla.org/fr/docs/Web/HTML/Element/span)


## Positionner des éléments avec `flexbox` et `grid`

Tout ce que nous avons vu jusqu'à présent ne permet de gérer le positionnement de plusieurs éléments sur une page, ce qui est indispensable sur la plupart des pages Web.

Pour positionner les éléments sur la page, il existe deux modèles natifs : "Flexbox" et "Grid Layout".

Pour découvrir et apprendre ces deux méthodes de positionnement, vous pouvez utiliser les jeux en ligne suivants qui vous guident pas à pas :

### Apprendre Flexbox avec Froggy la grenouille

Lien vers le jeu **Flexbox Froggy** : [https://flexboxfroggy.com/#fr](https://flexboxfroggy.com/#fr)

<a class="effacer-impression" href="https://flexboxfroggy.com/#fr" target="_blank"><img class="centre image-responsive" alt="Flexbox Froggy" src="data/flexboxfroggy.png" width="700"></a>
<p class="legende effacer-impression">Source : <a href="https://github.com/thomaspark/flexboxfroggy/">https://github.com/thomaspark/flexboxfroggy/</a>.</p>

<details class="effacer-impression deroulement-simple">
    <summary>Voir une courte vidéo de présentation du jeu</summary>

<div class="video-responsive">
    <div class="youtube_player centre" videoID="OmHPegyY3UU" width="560" height="315" theme="light" rel="1" controls="1" showinfo="1" autoplay="0"></div>
</div>
</details>

### Apprendre Grid Layout en cultivant des carottes

Lien vers le jeu **Grid Garden** : [https://cssgridgarden.com/#fr](https://cssgridgarden.com/#fr)

<a class="effacer-impression" href="https://cssgridgarden.com/#fr" target="_blank"><img class="centre image-responsive" alt="Grid Garden" src="data/cssgridgarden.png" width="700"></a>
<p class="legende effacer-impression">Source : <a href="https://github.com/thomaspark/gridgarden/">https://github.com/thomaspark/gridgarden/</a>.</p>

Il existe aussi des tutoriels intéressants :
- sur Flexbox :
    - de Graven (débutant) : [https://youtu.be/2qA4mobfcK0](https://youtu.be/2qA4mobfcK0)
    - de Grafikart : [https://grafikart.fr/tutoriels/flexbox-806](https://grafikart.fr/tutoriels/flexbox-806)
    - de SwebDev : [https://youtu.be/UcC76tcvLgA](https://youtu.be/UcC76tcvLgA)
- sur Grid Layout : [https://grafikart.fr/tutoriels/grid-css-1002](https://grafikart.fr/tutoriels/grid-css-1002)
- Float, Flex ou grid ? : [https://grafikart.fr/tutoriels/float-flex-grid-2017](https://grafikart.fr/tutoriels/float-flex-grid-2017)

## Sélectionner des éléments

Lorsqu'une page Web devient assez conséquentes, les balises deviennent très nombreuses, et imbriquées les unes dans les autres. Et cela devient plus compliqué pour sélectionner certains éléments en CSS. Pour apprendre à sélectionner les éléments, le jeu interactif **CSS Diner** est intéressant (en anglais) : [https://flukeout.github.io/#](https://flukeout.github.io/#).

<a class="effacer-impression" href="https://flukeout.github.io/#" target="_blank"><img class="centre image-responsive" alt="CSS Diner" src="data/cssdiner.jpg" width="700"></a>
<p class="legende effacer-impression">Source : <a href="https://github.com/flukeout/css-diner/">https://github.com/flukeout/css-diner/</a>.</p>

---

Germain Becker, Lycée Emmanuel Mounier, Angers.

![Licence Creative Commons](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)
