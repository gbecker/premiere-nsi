Défis Javascript
================

Dans cette activité vous allez apprendre à utiliser JavaScript pour créer des pages Web interactives. On y verra principalement comment gérer certains événements lors de clics sur des boutons. Cette activité est divisée en 4 parties. Dans chacune d’elles, une vidéo est à visionner puis plusieurs défis sont proposés. Votre objectif est de trouver une solution à ces défis !

# Modifier les styles d’une balise

Commencez par regarder la vidéo suivante.

<div class="video-responsive">
    <div class="youtube_player centre" videoID="8m6dRXQHOr4" width="560" height="315" theme="light" rel="1" controls="1" showinfo="1" autoplay="0"></div>
</div>
<p class="impression">
    Source : ▶️ <a href="https://youtu.be/8m6dRXQHOr4">https://youtu.be/8m6dRXQHOr4</a>
</p>

Si nécessaire, voilà un lien vers CodePen pour le code présenté dans la vidéo : <a href="https://codepen.io/gbecker/pen/VwLBdNQ" target="_blank">https://codepen.io/gbecker/pen/VwLBdNQ</a>.

<div class="a-faire titre" markdown="1">

### ✏️ Défi 1

Créez une page avec 3 boutons qui permettent de choisir la couleur de fond de la page parmi 3 couleurs.

*Vous ferez ce défi sur Capytale avec le code fourni par votre enseignant.*

</div>

# Changer le texte d’une balise

Commencez par regarder la vidéo suivante.

<div class="video-responsive">
    <div class="youtube_player centre" videoID="6WUJdEfftK8" width="560" height="315" theme="light" rel="1" controls="1" showinfo="1" autoplay="0"></div>
</div>
<p class="impression">
    Source : ▶️ <a href="https://youtu.be/6WUJdEfftK8">https://youtu.be/6WUJdEfftK8</a>
</p>

Si nécessaire, voilà un lien vers CodePen pour le code présenté dans la vidéo : <a href="https://codepen.io/gbecker/pen/jOPpgrw" target="_blank">https://codepen.io/gbecker/pen/jOPpgrw</a>.

<div class="a-faire titre" markdown="1">

### ✏️ Défi 2

Créez une page avec 2 boutons qui permettent d’afficher deux textes différents dans un paragraphe (le 1er bouton affiche un texte et le 2ème bouton en affiche un autre).

*Vous ferez ce défi sur Capytale avec le code fourni par votre enseignant.*

</div>

# Utiliser des variables simples

Commencez par regarder la vidéo suivante.

<div class="video-responsive">
    <div class="youtube_player centre" videoID="VgEgzbM4Oho" width="560" height="315" theme="light" rel="1" controls="1" showinfo="1" autoplay="0"></div>
</div>
<p class="impression">
    Source : ▶️ <a href="https://youtu.be/VgEgzbM4Oho">https://youtu.be/VgEgzbM4Oho</a>
</p>

Si nécessaire, voilà un lien vers CodePen pour le code présenté dans la vidéo : <a href="https://codepen.io/gbecker/pen/xxGagGd" target="_blank">https://codepen.io/gbecker/pen/xxGagGd</a>.

<div class="a-faire titre" markdown="1">

### ✏️ Défi 3

Créez un "Compteur de clic", c'est à dire une page avec 1 bouton et une zone d'affichage dont la valeur initiale est 0 et qui augmente de 1 à chaque clic. (Bonus : ajoutez un deuxième bouton de remise à zéro)

*Vous ferez ce défi sur Capytale avec le code fourni par votre enseignant.*

</div>

# Utiliser les données d'un champ de saisie

Commencez par regarder la vidéo suivante.

<div class="video-responsive">
    <div class="youtube_player centre" videoID="LE4SHWN2FWk" width="560" height="315" theme="light" rel="1" controls="1" showinfo="1" autoplay="0"></div>
</div>
<p class="impression">
    Source : ▶️ <a href="https://youtu.be/LE4SHWN2FWk">https://youtu.be/LE4SHWN2FWk</a>
</p>

Voilà un lien vers CodePen pour le code présenté dans la vidéo : <a href="https://codepen.io/gbecker/pen/GRJXWbr" target="_blank">https://codepen.io/gbecker/pen/GRJXWbr</a>.

Voilà un lien vers **replit** pour le code présenté dans la vidéo : <a href="https://replit.com/@beckerg/Video4UtiliserDonneesChampSaisieFINAL" target="_blank">https://replit.com/@beckerg/Video4UtiliserDonneesChampSaisieFINAL</a>.

<div class="a-faire titre" markdown="1">

### ✏️ Défi 4

Créez une page contenant un champ de saisie et un bouton. L’utilisateur entre son année de naissance et le bouton permet d’afficher son âge dans une zone d’affichage (un paragraphe par exemple).

*Vous ferez ce défi sur Capytale avec le code fourni par votre enseignant.*

</div>

# Défis supplémentaires

Voici quelques défis supplémentaires pour les plus rapides :

✏️ **Défi 5** : Créez une page avec un paragraphe dont la couleur du texte est modifiée à son survol.  
Bonus : le paragraphe doit revenir à son état initial à la fin du survol.  
*En lien avec la vidéo "Modifier les styles d'une balise".*

✏️ **Défi 6** : Créez une page avec un bouton permettant d’échanger le contenu de deux paragraphes.  
*En lien avec la vidéo "Changer le texte d’une balise".*

✏️ **Défi 7** : Créez une page avec un bouton et deux champs de saisie. Au clic sur le bouton, la somme des deux entiers entrés par l’utilisateur doit s’afficher dans une zone d’affichage (un paragraphe par exemple).  
*En lien avec la vidéo "Utiliser les données d'un champ de saisie".*

✏️ **Défi 8** : Créez une page avec un bouton et trois champs de saisie. Au clic sur le bouton, la couleur de fond de la page doit prendre la couleur RVB dont les composantes R, V et B ont été entrées par l’utilisateur. Exemple : si l’utilisateur entre 5, 121 et 143 dans les 3 zones de saisie, alors le fond de la page doit prendre la couleur `rgb(5,121,143)`.  
*En lien avec la vidéo "Utiliser les données d'un champ de saisie".*

Voici un lien vers les corrections des défis 7 et 8 :

- Défi 7 : https://replit.com/@beckerg/Defi7#index.html
- Défi 8 : https://replit.com/@beckerg/Defi8#index.html

---
Germain Becker, Lycée Emmanuel Mounier, Angers.

![Licence Creative Commons](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)
