Architecture d'un réseau - Exercices
====================================

# Exercice 1

✍️ **Question 1** : Un réseau possède un seul ordinateur d'adresse IP `192.168.5.17/24`. L'administrateur réseau doit en installer 3 autres. Donnez une adresse IP valide pour chacun d'eux.

✍️ **Question 2** : On considère le réseau ci-dessous formé de deux sous-réseaux de masque `/24`. Le routeur qui interconnecte les deux sous-réseaux possède une adresse IP dans chacun d'eux. Donnez une @IP possible pour ce routeur dans chaque réseau.

<img class="centre image-responsive" src="data/ex1_q2.png" alt="schéma du réseau">

# Exercice 2

Déterminez les adresses réseau à partir des adresses IP suivantes d'une machine du réseau :

- `147.12.1.24/16`
- `192.168.2.45/24`
- `5.23.65.87/8`
- `25.8.254.253/20`

# Exercice 3

Déterminez les adresses de broadcast à partir des adresses IP suivantes d'une machine du réseau :

- `147.12.1.24/16`
- `192.168.2.45/24`
- `5.23.65.87/8`
- `25.8.254.253/20`

# Exercice 4

✍️ **Question 1** : Combien de machines peut-on connecter sur un réseau dont le masque est `255.0.0.0` ?

✍️ **Question 2** : Combien de machines peut-on connecter sur un réseau dont le masque est `/20` ?

✍️ **Question 3** : On installe 10 machines sur un réseau local. L'adresse IP `172.16.29.35` avec le masque `255.255.255.240` (soit /28) est attribuée à une de ces machines.

1. Combien d'hôtes peut-on placer dans le réseau où est située cette machine ?
2. Dans quel réseau (préciser l'adresse) sont situées ces machines ?
3. Pour ce réseau, donnez la première et la dernière adresse IP valide pour ces machines.
4. Quelle est l'adresse de diffusion (broadcast) pour ce réseau ?

---
**Sources** :
- Exercices 2 et 3 proposés par David Roche : https://dav74.github.io/site_nsi_prem/c16a/

---
Germain Becker, Lycée Emmanuel Mounier, Angers.  
Sous licence [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.fr).

![Licence Creative Commons](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)