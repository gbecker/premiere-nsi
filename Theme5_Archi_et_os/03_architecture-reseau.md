Architecture d'un réseau
========================

Un **réseau informatique** est un ensemble de machines (de différentes sortes) interconnectées, de manière à ce que chacune puisse communiquer avec toutes les autres du réseaux.

Par exemple, Internet est un réseau mondial, mais ce n'est pas le seul : des entreprises ou des organisations peuvent posséder leur propre réseau informatique (relié ou non à Internet d'ailleurs), que l'on appelle alors *réseau local*.

En réalité, la plupart du temps lorsque l'on parle de *réseau*, il s'agit en réalité d'un **sous-réseau** d'un réseau plus grand (par exemple un sous-réseau du réseau mondial IP). Par exemple, Internet est constitué d'environ 100 000 sous-réseaux indépendants, chacun étant eux-mêmes constitués de plusieurs (beaucoup !) sous-réseaux.

# Les principales machines d'un réseau

Les *réseaux locaux* sont des (petits) réseaux constitués d'un ensemble de machines à l'échelle d'une maison, d'une entreprise, d'une organisaiton, d'un lycée, etc. Les machines d'un même réseau local peuvent communiquer directement entre elles.



Les machines d'un même sous-réseau sont généralement reliées par un **switch** (on dit **commutateur** en français), qui est un équipement informatique sur lequel on relie toutes les machines d'un réseau local grâce à des prises RJ45 femelles dans lesquelles on peut brancher des câbles Ethernet, aussi appelés “câbles réseau”.

<img class="centre image-responsive" src="data/switch1.jpg" alt="un switch">
<p class="legende">
    <strong>Fig. 1 - Un switch</strong>
    <br>Crédit : <a href="https://commons.wikimedia.org/wiki/File:Switch_reseau.jpg" target="_blank">KoS</a>, Domaine public, via Wikimedia Commons.
</p>

<img class="centre image-responsive" src="data/switch2.jpg" alt="un switch" width="300">
<p class="legende">
    <strong>Fig. 2 - Un switch à l'échelle d'une organisation</strong>
    <br>Crédit : Switch!, par <a target="_blank" rel="noopener noreferrer" href="https://www.flickr.com/photos/83711730@N06">Andrew Hart</a>, <a target="_blank" rel="noopener noreferrer" href="https://creativecommons.org/licenses/by-sa/2.0/?ref=openverse">CC BY-SA 2.0</a>, via Flickr.
</p>

Voici un schéma avec deux réseaux locaux :

<img class="centre image-responsive" src="data/reseaux_1_2.png" alt="un mini réseau Internet">
<p class="legende">
    <strong>Fig. 3 - Deux réseaux locaux</strong>
</p>

Dans ce schéma :

- Le switch S1 interconnecte les machines M1, M2 et M3
- Le swicth S2 interconnecte quant à lui M4, M5 et M6

Ainsi, M1, M2, M3 et S1 forment un sous-réseau, et M4, M5, M6 et S2 forment un autre sous-réseau.

Comme vous le verrez par la suite, deux machines d'un même (sous-)réseau peuvent communiquer directement entre elles. Mais deux machines appartenant à des réseaux différents ne peuvent pas communiquer directement.

Pour interconnecter deux réseaux entre eux, et permettre aux ordinateurs des deux réseaux de communiquer, on utilise des machines particulières appelées **routeurs**.

<img class="centre image-responsive" src="data/routeur.jpg" alt="un routeur" width="250">
<p class="legende">
    <strong>Fig. 4 - Un routeur Cisco CRS-1 (2004)</strong>
    <br>Crédit : Photo fournie par Cisco Systems Inc., <a href="http://creativecommons.org/licenses/by-sa/3.0/" title="Creative Commons Attribution-Share Alike 3.0">CC BY-SA 3.0</a>, via <a href="https://commons.wikimedia.org/w/index.php?curid=2621637">Wikimedia Commons</a></a>.
</p>

Pour relier nos deux réseaux précédents, on positionne un routeur A entre les deux :

<img class="centre image-responsive" src="data/reseaux_1_et_2.png" alt="un mini réseau Internet">
<p class="legende">
    <strong>Fig. 5 - Deux réseaux interconnectés par un routeur</strong>
</p>

Ce routeur appartient à la fois aux deux réseaux, car il possède deux cartes réseaux, chacune étant reliée à l'un des deux réseaux. Désormais, les machines du premier réseau peuvent communiquer avec celles du second, et réciproquement, en passant les messages au routeur A.

Ces réseaux locaux sont généralement des **sous-réseaux** d'un réseau plus grand :

<img class="centre image-responsive" src="data/mini_internet_bis.png" alt="un mini réseau Internet" id="mini-internet">
<p class="legende">
    <strong>Fig. 6 - Un réseau constitué de plusieurs sous-réseaux</strong>
</p>

Le schéma ci-dessus correspond à un "Internet très simplifié", où différents routeurs sont reliées entre eux pour interconnecter tous les (sous-)réseaux. Il existe généralement plusieurs chemins possibles entre deux machines, ce qui permet de palier à l'éventuelle panne d'un routeur.

<blockquote class="information" markdown="1">

Les routeurs se "débrouillent" pour acheminer de proche en proche les messages entre un émetteur et un récepteur : on appelle cela le **routage**. Les algorithmes de routage mis en oeuvre seront étudiés en détail en classe de Terminale. Nous n'en parlerons donc pas cette année.

</blockquote>

# Adresses IP

Chacun de ses (sous-)réseaux possède une adresse IP, appelée **adresse du (sous-)réseau**. Dans un (sous-)réseau, il y a plusieurs machines appelées **hôtes**, possédant elles aussi une adresse IP dépendant de celle du (sous-)réseau.

Une adresse **IPv4** (= IP version 4) est une suite de 4 octets (32 bits) : `X1.X2.X3.X4` où `X1`, `X2`, `X3` et `X4` sont les valeurs des 4 octets, exprimées en décimal ou en binaire.

<img class="centre image-responsive" src="data/Adresse_Ipv4.svg" alt="une adresse IPv4" width="400">
<p class="legende">
    <strong>Fig. 7 - Un exemple d'adresse IPv4</strong>
    <br>Crédit : <a href="https://commons.wikimedia.org/wiki/File:Adresse_Ipv4.svg">Star Trek Man</a>, Public domain, via Wikimedia Commons
</p>

<blockquote class="information" markdown="1">

Avec IPv4, il est possible de définir $2^{32} = 4\,294\,967\,296$ adresses IP distinctes, ce qui a longtemps suffit mais le nombre de machines reliées à Internet a désormais explosé, notamment à cause des objets connectés (bien plus nombreux que les ordinateurs). 

Une nouvelle norme, nommée **IPv6**, est progressivement utilisée car elle permet d'écrire des adresses IP sur 128 bits (au lieu de 32 pour IPv4). Cette norme permet de connecter environ 340 milliards de milliards de milliards de machines ($2^{128} \simeq$ 340 000 000 000 000 000 000 000 000 000 000 000 000) ce qui sera largement suffisant pour le futur d'Internet et des objets connectés.

</blockquote>

## Structure d'une adresse IP : parties *réseau* et *machine*

Pour chaque adresse, une partie des bits (les bits de poids fort, à gauche) représente la partie « réseau », et l'autre partie (les bits de poids faible, à droite) représente la partie « machine » (ou *hôte*).

Par exemple, si les 3 premiers octets représentent la partie *réseau* et le dernier la partie *hôte*, on obtient le découpage suivant :

| réseau | . | machine |
| ---: | --- | :--- |
| 11000001 00110111 11011101 | . | 00111110 |
| 193.55.221 | . | 62 |

Ainsi, si deux machines appartiennent au même réseau, elles possèdent la même partie *réseau* mais une partie *machine* différente. Nous allons voir comment tout cela est déterminé.

## Masque de sous-réseau

Le mécanisme permettant de situer la limite en la partie *réseau* et la partie *machine* s'appelle le **masque de sous-réseau** (en anglais : *subnet mask*). C'est aussi une suite de 32 bits :

- une série continue de `1` qui fixe la partie *réseau* (celle de gauche)
- une série continue de `0` qui correspond à la partie *machine*

**Exemple** : Dans le cas où les 3 premiers octets représentent la partie *réseau*, alors le masque de sous-réseau est `11111111.11111111.11111111.00000000` ou `255.255.255.0` en décimal.

Si M1 possède l'adresse IP `192.168.1.1` et que le masque de sous-réseau est `255.255.255.0`, cela signifie que la partie réseau correspond aux trois premiers octets (c'est donc `192.168.1`) et que le dernier octet sert à identifier les différentes machines connectées sur ce réseau. Ainsi, toutes les machines connectées à ce réseau ont une adresse IP qui commence nécessairement par `192.168.1`. Par exemple, M2 et M3 peuvent avoir pour adresses respectives `192.168.1.2` et `192.168.1.5`.

<div markdown="1" class="a-faire">

**Ai-je compris ?**

*On reprend ici le réseau de la [figure 6](#mini-internet) du paragraphe précédent*

✍️ **Question 1** : Si M4 a pour adresse IP `192.168.2.3` et que le réseau auquel elle appartient a pour masque `255.255.255.0`, donnez une adresse possible pour M5 et M6.

✍️ **Question 2** : Si M20 a pour adresse IP `195.32.12.17` sur un réseau dont le masque est `255.255.0.0`, donnez une adresse possible pour les autres machines du réseau.

</div>

## Notation CIDR

Le masque de sous-réseau peut s'écrire en notation binaire ou décimale mais on utilise généralement une notation simplifiée, appelée **notation CIDR** (de l'anglais *Classless Inter-Domain Routing*). Cette notation utilise un slash suivi du nombre de bits correspondant à la partie *réseau*.

**Exemple** : le masque de sous-réseau `255.255.255.0` (soit `11111111.11111111.11111111.00000000` en binaire) s'écrit donc plus simplement `/24` car il y a 24 bits pour la partie *réseau*.

<div markdown="1" class="a-faire">

**Ai-je compris ?**

✍️ **Question 1** : Déterminez la notation CIDR du masque `255.255.0.0`.

✍️ **Question 2** : Déterminez la notation CIDR du masque `255.255.255.240`.

</div>


<blockquote class="information" markdown="1"> 

Les masques classiques sont `/24`, `/16` et `/8` mais rien n'empêche d'avoir un masque qui ne découpe pas les parties *réseau* et *machine* sur des octets complets.

</blockquote>

# Adresses au sein d'un réseau

## Adresse réseau

L'**adresse réseau** (d'un réseau) est l'adresse IP caractérisant ce réseau. C'est tout simplement l'adresse IP dont tous les bits de la partie *machine* sont à 0, autrement dit l'adresse IP la plus "basse" du réseau.

>L'**adresse réseau** permet de savoir si 2 machines peuvent communiquer entre elles (directement). Si ces 2 machines ont une adresse réseau identique, alors elles appartiennent au même réseau et elles peuvent communiquer.

**Exemple** : si `193.55.221.62` (qui donne `11000001.00110111.11011101.00111110` en binaire) est une machine d'un réseau dont le masque est `/24`, alors l'adresse du réseau en question est :

- `11000001.00110111.11011101.00000000` les 8 derniers bits pour l'hôte ont été mis à zéro
- en décimal cela donne `193.55.221.0`.

Ainsi, le réseau en question se note `193.55.221.0 /24`.

Les adresses des machines d'un réseau sont généralement données avec le masque de sous-réseau. Par exemple, au lieu d'écrire qu'une machine a pour adresse `193.55.221.62` dans un réseau dont le masque est `/24`, on écrira de manière plus synthétique que cette machine a pour `193.55.221.62 /24` (on précise directement le masque avec la notation CIDR).

### Utilisation du ET logique

Si on connaît l'adresse d'une machine ainsi que le masque, on peut trouver l'adresse réseau en effectuant un ***ET*** logique bit à bit entre l'adresse machine et le masque de sous-réseau.

Ainsi, pour une machine d'adresse `193.55.221.62` (soit `11000001.00110111.11011101.11111000` en binaire), dans un réseau de masque `/24` (soit `11111111.11111111.11111111.00000000` en binaire), on obtient l'adresse réseau ainsi :

```
   11000001.00110111.11011101.11111000
ET 11111111.11111111.11111111.00000000
   -----------------------------------
   11000001.00110111.11011101.00000000  <-- adresse réseau
```

<blockquote class="information" markdown="1"> 

Cette méthode peut semble "inutile" pour des masques simples comme `/24`, `/16` ou `/8` qui correspondent pile à des octets, mais s'avère très utiles lorsque les masques sont différents, comme `/18` par exemple (voir exercices).

</blockquote>

## Adresse de diffusion

L'**adresse de diffusion** ou **broadcast** est celle utilisée pour envoyer des paquets à *toutes* les machines du réseau. C'est l'adresse dont les bits de la partie *machine* sont tous à 1, autrement dit l'adresse IP la plus "haute" du réseau.

**Exemple** : Le réseau `193.55.221.0 /24` a donc pour adresse de diffusion `11000001 00110111 11011101 11111111` ou `193.55.221.255` en décimal (la partie *machine* correspond au dernier octet, dans lequel on a mis tous les bits à 1).

## Plage d'adresses machines

La plage d'adresses machines est l'ensemble des adresses que peut prendre une machine sur le réseau. Comme l'adresse réseau et l'adresse de diffusion sont déjà prises, il restent toutes les adresses entre les deux pour les *machines* du réseau :

- la première adresse possible pour une machine est donc celle qui suit l'adresse réseau
- la dernière adresse possible pour une machine est donc celle qui précède l'adresse de diffusion

**Exemple** : Sur le réseau `193.55.221.0 /24`, seul le dernier octet correspond à la partie *machine*. Or, on sait que l'adresse réseau est `193.55.221.0` et que l'adresse de broadcast est `193.55.221.255`, il ne reste donc que les 254 autres adresses possibles ($256 - 2 = 254$) pour les machines : leurs adresses IP peuvent donc varier de `193.55.221.1` (la première) à `193.55.221.254` (la dernière).

<div markdown="1" class="a-faire">

**Ai-je compris ?**

✍️ **Question 1** : Une machine d'adresse IP `112.133.102.41` appartient à un réseau dont le masque est `/24` (c'est-à-dire `255.255.255.0`). 
1. Donner en notation binaire et en notation décimale, l'adresse réseau et l'adresse de diffusion du réseau.
2. Combien de machines peut-on connecter dans ce réseau ? Quelle est leur plage d'adresses ?

✍️ **Question 2** : Combien de machines peut-on connecter sur un réseau dont le masque est `/16` ?

</div>

# Bilan

- Un **réseau informatique** est un ensemble d'équipements reliés entre eux pour échanger des informations. En réalité un réseau est souvent constitué d'une multitude de **sous-réseaux**.
- Les ordinateurs d'un même (sous-)réseau peuvent communiquer entre elles directement car elles ils sont reliés par un **switch** (ou *commutateur*). Mais si une machine veut communiquer avec une machine située dans un autre (sous-)réseau, alors il faut que ces deux réseaux soient reliés par un **routeur**.
- Le réseau Internet est donc un immense réseau formé de centaines de milliers de sous-réseaux reliés entre eux par un nombre très important de routeurs qui sont chargés d'acheminer les messages échangés en trouvant un chemin de proche en proche jusqu'au destinataire.
- Les communications se font via les **adresses IP** des machines source et destination. Dans un réseau, une adresse IP (v4) est composée de deux parties : une partie *réseau*, commune à tous les ordinateurs du réseau, et une partie *machine* qui identifie la machine dans ce réseau. 
- C'est le **masque de sous-réseau** qui permet de déterminer la limite entre les parties *réseau* et *machine*. Le masque est souvent donnée avec sa *notation CIDR* pour simplifier les écritures.
- Lorsque les bits de la parties *machine* valent tous 0, on obtient l'**adresse réseau** (du réseau); et lorsqu'ils valent tous 1, on obtient l'**adresse de diffusion** (du réseau) aussi appelée **broadcast**, qui est celle à utiliser pour envoyer un message à toutes les machines du réseau. 
- Toutes les autres valeurs de la partie *machine* de l'adresse IP peuvent être attribuées aux différentes machines du réseau.
- La connaissance de l'adresse IP d'une machine et du masque de sous-réseau permet de déterminer l'adresse réseau de la machine. En particulier, cela permettra de savoir si la machine à contacter se trouve ou non dans le même réseau que l'émetteur : dans l'affirmative le message pourra être envoyé directement, et dans le cas contraire il faudra l'envoyer à un routeur pour sortir du réseau.

---

**Références** :
- Cours *Introduction aux réseaux* du DIU EIL de l'université de Nantes, Pierrick Passard et Salima Hamma.
- Cours de SNT sur la communication entre deux machines sur Internet : https://info-mounier.fr/snt/internet/protocole-tcp-ip.php
- Cours d'introduction aux réseaux de David Roche  : https://dav74.github.io/site_nsi_prem/c16c/

---

Germain Becker, Lycée Emmanuel Mounier, Angers.  
Sous licence [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.fr).

![Licence Creative Commons](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)