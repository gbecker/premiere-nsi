🎮 Jeu Terminus
============

# Introduction

<blockquote class="information">
    <p>La version originale de cette activité a été proposée par Charles Poulmaire.</p>
</blockquote>

Cette activité vous permet de découvrir un certain nombre de commandes UNIX, utilisables dans le Shell (ou Terminal) en jouant à un jeu en ligne appelé **Terminus**.

**Vous trouverez ce jeu à l'URL suivante : <a href="https://luffah.xyz/bidules/Terminus/" target="_blank">https://luffah.xyz/bidules/Terminus/</a>**

# ✍️ Travail à faire

Jouez à Terminus et complétez **au fur et à mesure** :
- un tableau des commandes découvertes avec leur fonctionnalité et leur syntaxe,
- une représentation du monde de TERMINUS sous la forme d'une arborescence, comme celle d'un système de fichiers.

Voici un exemple avec les premières commandes découvertes dans le jeu :

|Commande|Description|Syntaxe|
|:---:|:---|:---|
|`cat`|Afficher dans la console|Saisir `cat Objet` ou `cat Personne`|
|`ls`|Lister les éléments|Saisir `ls`|
|`cd`|Changer de Destination|<div>`cd ..` pour revenir à l'emplacement précédent<br>`cd  Salle` pour entrer dans Salle<br>`cd  ~` pour revenir au point de départ</div>|
| ... | ... | ... |

Voici une façon de faire un plan du jeu :

<img class="centre image-responsive" alt="début du plan du jeu Terminus" src="data/plan-terminus.png">

><span style="font-size:1.5em">⚠️</span> Ce jeu est long ! Aussi, **pour conserver votre progression**, et reprendre le jeu à l'endroit où vous vous étiez arrêté, il est nécessaire d'**accepter les cookies** au début du jeu.


<div class="zone-telechargement">
    <h1 class="titre-zone-telechargement not_before">Pour jouer en local</h1>
    <p>Pour jouer en local, téléchargez le fichier compressé suivant, puis décompressez-le et ouvrez la page <code>Terminus.html</code> avec un navigateur Web : <a href="data/Terminus.zip" download target="_blank">Terminus.zip</a>.</p>
</div>


---
Germain Becker & Sébastien Point, Lycée Emmanuel Mounier, Angers.  
Sous licence [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/deed.fr).

![Licence Creative Commons](https://i.creativecommons.org/l/by/4.0/88x31.png)

