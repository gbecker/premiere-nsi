TP : Visualiser les protocoles de communication
===============================================

Dans ce TP vous allez utiliser le logiciel Filius pour visualiser les différents protocoles utilisés lors d'un échange entre deux machines.



# Observation des trames

Dans cette première partie, nous allons travailler avec le réseau suivant :

<img class="centre image-responsive" src="data/observation_arp.png" alt="Schéma du réseau">
<p class="legende">
    <strong>Fig. 1 - Réseau pour observer les premières trames</strong>
</p>

<div class="a-faire" markdown="1">

**Téléchargement**

Commencez par télécharger le fichier <a href="data/arp.fls" target="_blank" download>arp.fls</a> en question et ouvrez-le avec Filius.

</div>

L'objectif ici est de voir comment observer les trames échangées entre machines, les protocoles mis en jeu au niveau des différentes couches du *modèle Internet* (Filius se base sur ce modèle et non le modèle OSI).

## Rôle du protocole ARP

L'ordinateur `192.168.0.1`, d'adresse MAC `06:5C:09:34:6E:32` a déjà le logiciel *Ligne de commandes* installé.

💻 **Question 1** : Passez en *mode simulation* (flèche verte) puis ouvrez le terminal sur l'ordinateur 192.168.0.1. Exécutez la commande `arp -a` pour voir la *table ARP* qui contient les correspondances entre @IP et @MAC. Vous devez voir que cette table est vide pour le moment (seule l'adresse de diffusion est présente).

<img class="centre image-responsive" src="data/table_arp_vide.png" alt="table ARP vide">

💻 **Question 2** : Laissez le logiciel "Ligne de commandes" ouvert et cliquez droit sur l'ordinateur `192.168.0.1` puis cliquez sur "Afficher les échanges de données ..." qui ouvre une fenêtre dans laquelle on va voir les différentes trames échangées par cet ordinateur. Faites ensuite un ping vers `192.168.0.3` da. Cette commande déclenche l'échange de plusieurs trames comme sur la capture ci-dessous :

<img class="centre image-responsive" src="data/arp_ping.png" alt="différentes requêtes et réponses">

### Requête et réponse ARP

On va s'intéresser aux deux premiers messages échangés.

### Premier message : « Qui possède l'@IP `192.168.0.3` ? »

Le premier paquet échangé via le protocole ARP, s'appelle la **requête ARP**. En cliquant sur ce paquet on obtient le détail :

<img class="centre image-responsive image-encadree" src="data/requete_arp_zoom.png" alt="détail de la requête ARP">

**Analyse** :

- La couche "Internet" indique bien que cette requête a été envoyée par `192.168.0.1` à `192.168.0.3` (c'est notre `ping`) via le protocole ARP. Ces données ont été encapsulées par la couche "Réseau"
- Comme on l'a évoqué dans le cours, l'envoi physique d'une trame se fait via l'adresse MAC des machines du réseau. Comme la machine source ne connait pas l'@MAC de la machine destinataire, elle envoie un message à *toutes* les machines du réseau pour demander à `192.168.0.3` de lui renvoyer son @MAC.
- C'est ce qu'on voit dans la couche "Réseau" (diminutif d'accès réseau, comme on l'a appelée dans le cours) : le message a été envoyé à l'@MAC de diffusion (ou broadcast) `FF:FF:FF:FF:FF:FF`.

<blockquote class="information" markdown="1">

Vous pouvez observez que les deux autres machines du réseau ont reçu cette requête ARP (clic droit sur celles-ci puis clic sur "Afficher les échanges de données") mais seule `192.168.0.3`, qui s'est reconnue a repondu.

</blockquote>

### Deuxième message : « C'est moi ! »

Le second message s'appelle la **reponse ARP** : la machine cherchée `192.168.0.3` a renvoyé son @MAC à `192.168.0.1` pour lui indiquer que c'est elle qu'elle veut contacter :

<img class="centre image-responsive image-encadree" src="data/reponse_arp_zoom.png" alt="détail de la réponse ARP">

✍️ **Question 3** : Analysez la réponse ARP pour répondre aux questions suivantes : 

1. Justifiez que cette réponse n'est pas envoyée à toutes les machines du réseau.
2. À qui est-elle envoyée ? Comment `192.168.0.3` connait-il l'@MAC de `192.168.0.1` ?

À partir de maintenant, `192.168.0.1` connaît l'@MAC de `192.168.0.3` et l'a enregistré dans sa table ARP qui n'est plus vide (jusqu'à effacement de la mémoire cache) :

<img class="centre image-responsive" src="data/table_arp_completee.png" alt="table ARP mise à jour">

<blockquote class="information" markdown="1">

Vous pouvez vérifier qu'en faisant le même ping que précédemment, `192.168.0.1` n'a plus besoin d'envoyer de requête ARP à tout le réseau puisqu'il regarde d'abord dans sa table ARP et se rend compte qu'il connaît l'@MAC de `192.168.0.3`. Cela permet évidemment un gain en temps, même si cela reste minime, et évite d'encombrer inutilement le réseau.

</blockquote>

<details markdown="1" class="effacer-impression en-savoir-plus">
    
<summary>Table SAT du switch</summary>

Par ailleurs, en cliquant sur le switch, vous pouvez rendre compte que les échanges entre nos deux machines, qui sont passés par le switch, lui ont permi d'enregistrer dans sa table les adresses MAC de `192.168.0.1` et `192.168.0.3` en les associant au bon port (cette table était vide au départ).

<img class="centre image-responsive" src="data/table_sat.png" alt="table SAT du switch">

En effet, un switch est un équipement qui se situe au niveau "Réseau" du modèle Internet et ne travaille donc pas avec les @IP mais avec les @MAC.

</details>

### Messages suivants : jeu de ping pong

Vous avez du constater que lors d'un `ping`, 4 messages sont envoyés au destinataire et on est en attente des 4 réponses (pong, en guise d'accusé de réception).

Les trames suivantes échangées par `192.168.0.1` correspondent à ce jeu de *ping pong* entre les deux machines. C'est le protocole ICMP qui se charge de cela, comme vous pouvez le voir avec Filius.

💻✍️ **Question 4** : Cliquez sur la troisième trame (le premier "ping") pour en voir le détail et complétez le schéma ci-dessous avec les bonnes informations pour chaque couche.

<img class="centre image-responsive" src="data/schema_a_completer_2_couches.svg" alt="schéma d'encapsulation des données">

💻✍️ **Question 5** : Cliquez sur la quatrième trame (le premier "pong") pour en voir le détail et complétez le schéma ci-dessous avec les bonnes informations pour chaque couche.

<img class="centre image-responsive" src="data/schema_a_completer_2_couches.svg" alt="schéma d'encapsulation des données">

💻✍️ **Question 6** : Pour différencier les différents "ping", un numéro de séquence est attribué. Vérifiez que ce numéro est bien itéré pour chaque "ping" et que le "pong" contient ce numéro également. Pourquoi à votre avis ?

# Requête HTTP vers un serveur

On va maintenant analyser les trames correspondants à une requête HTTP vers un serveur Web.

Pour cela, on va repartir du réseau construit au cours du TP du chapitre 3 sur la simulation d'un réseau : 

<img class="centre image-responsive" src="data/tp_reseau.png" alt="Schéma du réseau">
<p class="legende">
    <strong>Fig. 2 - Réseau pour les requêtes HTTP</strong>
</p>


<div class="a-faire" markdown="1">

**Téléchargement**

Commencez par télécharger le fichier <a href="data/tp_protocoles.fls" target="_blank" download>tp_protocoles.fls</a> en question et ouvrez-le avec Filius.

</div>

## Mise en place du serveur web

On va mettre en place un serveur dans le réseau R2 (celui d'adresse `192.168.1.0`). C'est la machine `192.168.1.3` qui jouera le rôle de serveur.

💻 **Question 7** : Passez en *mode conception* (flèche verte) et installez sur `192.168.1.3` les logiciels "Serveur web", "Explorateur de fichiers" et "Éditeur de textes".

<img class="centre image-responsive" src="data/installation_logiciels_serveur.png" alt="capture d'écran" width="500">

En ouvrant l'explorateur de fichiers on peut accéder aux fichiers présents sur le serveur : c'est le répertoire `root/webserver` qui contient les pages web hébergées par le serveur (ainsi que tous les autres fichiers). En particulier, `index.html` est celui qui sera envoyé au client qui effectue une requête à la racine du serveur.

<img class="centre image-responsive" src="data/serveur_explorateur_fichiers.png" alt="capture d'écran" width="500">

💻 **Question 8** : Lancez l'*éditeur de texte* puis ouvrez le fichier `index.html` pour en voir le contenu (il faut double-cliquer sur le fichier puis cliquer sur le bouton "Ouvrir"). Modifiez le contenu du `body` avec le code que vous souhaitez (un exemple ci-dessous) et enregistrer les modifications.

<img class="centre image-responsive" src="data/index_serveur.png" alt="capture d'écran" width="500">

💻 **Question 9** : Ouvrez enfin le logiciel "Serveur web" et cliquez sur "Démarrer" pour lancer le serveur qui est désormais prêt à recevoir les requêtes des clients. Vous devriez obtenir l'écran ci-dessous si tout s'est bien passé.

<img class="centre image-responsive" src="data/serveur_demarre.png" alt="capture d'écran" width="500">

## Requêtes du client

C'est l'ordinateur `192.168.0.1` qui jouera le rôle de client (cela pourrait être n'importe quel ordinateur en fait).

💻 **Question 10** : En *mode conception* (flèche verte), cliquez droit sur `192.168.0.1` et cliquez sur "Afficher les échanges de données". Vous devriez voir apparaître différentes trames utilisant le protocole RIP (programme de Terminale). On voit bien en cliquant sur l'une d'elles les différentes couches du modèle "Internet" comme ci-dessous. *Quelle machine a envoyé ces requêtes ? À qui ? Expliquez.*

<img class="centre image-responsive image-encadree" src="data/requete_presence_routeur.png" alt="détails de la requête de présence du routeur">


<blockquote class="information" markdown="1">

Un routeur envoie régulièrement à toutes les machines de son réseau un message pour signaler sa présence, et que l'on peut passer par lui (passerelle) pour contacter des machines en dehors du réseau. Cela correspond aux différents messages RIP.

</blockquote>

Vous pouvez effacer ces requêtes en cliquant droit puis "Vider les tables" (d'autres apparaîtront au fur et à mesure mais il y aura moins à défiler pour voir les trames qui nous intéressent par la suite)

💻 **Question 11** : Tout en laissant la fenêtre "Échanges de données", installez sur `192.168.0.1` le "Navigateur web" puis ouvrez-le et saisissez l'@IP du serveur dans la barre d'URL, soit `192.168.1.3`. La page `index.html` du serveur écrite à la question 8 doit s'afficher dans le navigateur.

<img class="centre image-responsive" src="data/client_navigateur.png" alt="capture d'écran" width="500">

Dans la fenêtre "Échanges de données", vous devez obtenir quelque chose de similaire à la capture d'écran ci-dessous.

<img class="centre image-responsive" src="data/client_requetes.png" alt="capture d'écran">

Dans la suite on ne s'intéresse qu'aux trames échangées suite à la validation de l'URL dans le navigateur c'est-à-dire à toutes sauf celles utilisant le protocole RIP :

<img class="centre image-responsive" src="data/client_requete_reponse_http.png" alt="capture d'écran">

## Analyse des deux premiers paquets ARP

✍️ **Question 12** : En utilisant ce qui a été vu dans la partie précédente, expliquez le rôle des deux premières trames utilisant le protocole ARP. *Soyez précis : qui envoie ? à qui ? dans quel but ?*

## Établissement et terminaison d'une connexion TCP

Lors d'un échange entre un client et un serveur via la protocole TCP, il est nécessaire d'établir une **session TCP** entre les deux machines. Cela fonctionne en trois phases :

1. l'établissement de la connexion
2. les transferts de données
3. la fin de la connexion

<img class="centre image-responsive" src="data/session_tcp.png" alt="les trois phases de la session TCP">

On évoque les première et dernière phases tout de suite avant de se concentrer sur le transfert de données pour terminer.

### Établissement et fin de la connexion TCP

L'**établissement de la connexion** se fait en trois temps (SYN, SYN+ACK, ACK):

- Le client envoie une demande de synchronisation au serveur (segment SYN, pour un établissement de connexion),
- Le serveur lui répond par un segment SYN/ACK,
- Le client confirme par un segment ACK.

<img class="centre image-responsive" src="data/demarrage_session_tcp.svg" alt="fin d'une session tcp en 4 temps">
<p class="legende">
    <strong>Fig. 3 - Établissement d'une connexion TCP en 3 temps</strong>
    <br>Crédit : Adaptation personnelle d'une image de <a href="https://commons.wikimedia.org/wiki/File:Tcp-handshake.svg">Snubcube</a>, <a href="http://creativecommons.org/licenses/by-sa/3.0/">CC BY-SA 3.0</a>, via Wikimedia Commons.
</p>

<blockquote class="information" markdown="1">

Pendant la phase d'établissement de la connexion, des paramètres comme le numéro de séquence sont initialisés afin d'assurer la transmission fiable (sans perte et dans l'ordre) des données.

</blockquote>

✍️ **Question 13** : Complétez le schéma ci-dessous correspondant à la première trame (la demande de synchronisation). Expliquez pourquoi l'@MAC de destination n'est pas celle du serveur.

<img class="centre image-responsive" src="data/schema_a_completer_3_couches.svg" alt="schéma d'encapsulation des données">

Les deux autres trames suivent le même principe comme vous pouvez le constatez par vous-même.

<blockquote class="information" markdown="1">

Le client envoie la trame à son routeur, qui désencapsule la couche "Internet" pour lire l'@IP de destination (celle du serveur). Il constate dans notre cas que le serveur est dans le même réseau que son interface `192.168.1.254`, diminue la valeur TTL (*Time To Live*) d'une unité (elle passe de 64 à 63) et envoie la trame au serveur, en ayant éventuellement au préalable demandé son @MAC via le protocole ARP. Vous pouvez d'ailleurs observer que le TTL vaut 63 lorsqu'il arrive au serveur en affichant les échanges de données de ce dernier.

Si le serveur n'était pas dans le même réseau que le routeur, ce dernier aurait passé la trame à un routeur voisin (selon un protocole étudié en Terminale). Chaque routeur ainsi traversé, décremente la valeur TTL d'une unité et si cette valeur devient égale à 0, le paquet est détruit pour éviter l'encombrement des réseaux avec des paquets qui ne trouvent pas leur destination.

</blockquote>

La **fin de la connexion** se fait, elle, en quatre temps avec des demandes de fin de connexion (FIN) et accusés de réception (ACK) de part et d'autre.

<img class="centre image-responsive" src="data/fin_session_tcp.svg" alt="fin d'une session tcp en 4 temps">
<p class="legende">
    <strong>Fig. 4 - Fin d'une connexion TCP en 4 temps</strong>
</p>

### Transfert des données via HTTP

Entre l'établissement et la rupture de la session TCP, a lieu la requête et la réponse HTTP comme on l'a vu dans le chapitre [Dialogue client-serveur sur le Web](/premiere_nsi/web/dialogue_client_serveur) du thème n°3. 

On va terminer en analysant les trames échangées entre le client et le serveur suite à la demande de la page Web par le client.

💻✍️ **Question 14** : Identifiez la requête HTTP ainsi que la réponse HTTP dans la série de trames échangées. Quel est le rôle de deux autres trames (protocole TCP) de la phase de transfert des données ?

✍️ **Question 15** : Complétez le schéma suivant avec les informations de la *requête HTTP*. En particulier, vous indentifierez la *ligne de commande* et les *en-têtes* de la requête HTTP. 

<img class="centre image-responsive" src="data/schema_a_completer_4_couches_ter.svg" alt="schéma d'encapsulation des données">

✍️ **Question 16** : Complétez le schéma suivant avec les informations de la *réponse HTTP*. En particulier, vous indentifierez la *ligne de statut*, les *en-têtes* et le *corps* de la réponse HTTP.

<img class="centre image-responsive" src="data/schema_a_completer_4_couches_bis.svg" alt="schéma d'encapsulation des données">


# Serveur DNS (bonus)

Vous avez constaté que le client a contacté le serveur en utilisant son @IP (dans la barre d'URL du navigateur). En réalité, on utilise le *nom de domaine* et c'est un **serveur DNS** qui permet d'associer au nom de domaine l'adresse IP du serveur correspondant.

Pour mettre en place un tel serveur DNS et comprendre le fonctionnement, vous pouvez regarder la vidéo suivante (de David Roche) qui présente la manière de procéder. Libre à vous de la mettre en oeuvre avec Filius et d'observer les différentes trames échangées.

<div class="video-responsive">
    <div class="youtube_player centre" videoID="EZp_TLGVyv0" width="560" height="315" end="343" theme="light" rel="1" controls="1" showinfo="1" autoplay="0"></div>
</div>
<p class="impression">
    Source : <a href="https://youtu.be/EZp_TLGVyv0" target="_blank">https://youtu.be/EZp_TLGVyv0</a>.
</p>

---

**Références** :
- Cours *Introduction aux réseaux* du DIU EIL de l'université de Nantes, Pierrick Passard et Salima Hamma.
- Articles Wikipédia : [Transmission Control Protocol](https://fr.wikipedia.org/wiki/Transmission_Control_Protocol), 
- Les cours suivants ont servi à construire ce TP :
    - cours de Gilles Lassus sur les [protocoles de communication](https://glassus.github.io/premiere_nsi/T3_Architecture_materielle/3.4_Protocoles_de_communication/cours/)
    - activité de David Roche sur la [simulation réseau](https://dav74.github.io/site_nsi_prem/c27a/)

---

Germain Becker, Lycée Emmanuel Mounier, Angers.  
Sous licence [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.fr).

![Licence Creative Commons](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)