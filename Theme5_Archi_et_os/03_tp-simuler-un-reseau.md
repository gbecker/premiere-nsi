TP : Simuler un réseau
======================

Comme il est compliqué de mettre en oeuvre réellement un réseau, vous allez, dans ce TP, utiliser le logiciel **Filius** pour simuler un réseau.

<blockquote class="information" markdown="1">

Le logiciel Filius est un simulateur facile à prendre en main et suffisamment complet pour nos besoins en NSI. Il s'agit d'un logiciel libre (licence GPL) et multiplateforme développé en Java par l'allemand Stefan Freischlad dans un but éducatif.  
Site officiel : https://www.lernsoftware-filius.de/Herunterladen  
Code source : https://gitlab.com/filius1/filius

</blockquote>

# Créer un premier réseau

💻 **Question 1** : Construisez le réseau R1 suivant constitué de 3 machines dont les adresses IP sont données et ayant un masque /24, soit 255.255.255.0.

<img class="centre image-responsive" src="data/filius_reseau_1.png" alt="premier réseau">
<p class="legende">
    <strong>Fig. 1 - Réseau R1 constitué de 3 ordinateurs.</strong>
</p>

Cliquez sur le triangle vert pour passer en mode simulation. Cliquez ensuite sur l'ordinateur 192.168.0.1 pour ouvrir une fenêtre permettant d'installer et d'utiliser des logiciels sur cet ordinateur. Cliquez sur l'icône "Installation de logiciels" puis installez le logiciel "Ligne de commande" en le faisant passer dans la colonne de gauche puis cliquez sur "Appliquer les modifications".

<img class="centre image-responsive" src="data/installation_logiciels.png" alt="installation des logiciels">

L'icône d'un terminal doit apparaître sur le bureau. Cliquez dessus pour ouvrir ce logiciel. 

## Quelques commandes

Pour connaître la configuration réseau d'une machine on peut utiliser la commande `ipconfig` (sous Windows).

💻 **Question 2** : Exécutez la commande `ipconfig` et vérifiez que les informations affichées sont correctes.

<img class="centre image-responsive" src="data/ipconfig.png" alt="ipconfig">

<blockquote class="information" markdown="1">

La commande `ipconfig` est propre à Windows. Un équivalent Linux est la commande `ifconfig`. 

</blockquote>

Pour tester si une machine est joignable on peut utiliser la commande `ping`, suivie de l'adresse IP de la machine.

💻 **Question 3** : Faites un `ping` vers la machine 192.168.0.2 en exécutant la commande `ping 192.168.0.2`. Vous devez constater des échanges entre les deux machines symbolisés par un changement de couleur des câbles de connexion. Vérifiez que les 4 paquets envoyés ont bien été reçus par 192.168.0.2.

<img class="centre image-responsive" src="data/ping.png" alt="un ping">

💻 **Question 4** : Faites un `ping` vers une machine inexistante, par exemple 192.168.0.50, et observez le résultat.

# Connecter un second réseau à l'aide d'un routeur

On souhaite maintenant ajouter un second réseau R2 de 3 machines également. L'une d'elle a pour adresse IP 192.168.1.1/24.

<img class="centre image-responsive" src="data/reseau_R2.png" alt="réseau R2">
<p class="legende">
    <strong>Fig. 2 - Réseau R2 constitué de 3 ordinateurs.</strong>
</p>

💻✍️ **Question 5** : Repassez en *mode conception* 🔨 (icône "marteau"). Créez ce nouveau réseau R2 en donnant des adresses IP cohérentes aux deux autres machines (il faudra relier ces trois machines pas un nouveau switch S2). Quelles sont les adresses IP choisies pour les deux autres machines de ce second réseau ?

## De la nécessité d'utiliser un routeur

💻✍️ **Question 6** : Reliez les deux switchs par un simple câble (Ethernet) et passez en *mode simulation* puis ouvrez à nouveau le terminal de l'ordinateur 192.168.0.1. Faites un `ping` vers la machine 192.168.1.1 pour tenter de la joindre. *Expliquez le message affiché.*

<blockquote class="information" markdown="1">

On voit que deux ordinateurs ne peuvent pas communiquer directement entre eux s'ils ne sont pas sur le même réseau. Seules deux machines du même réseau peuvent se voir. Pour permettre aux (machines des) deux réseaux de communiquer, il est nécessaire d'utiliser un **routeur**.

Ce routeur possèdera deux adresses : l'une dans le réseau R1 et l'autre dans le réseau R2. Il sera donc vu des ordinateurs des deux réseaux et sera ainsi à même d'acheminer les informations d'un réseau à l'autre. 

</blockquote>


## Configuration du routeur

💻 **Question 7** : Ajoutez un routeur entre les deux réseaux en sélectionnant 2 interfaces pour le routeur. Dans l'onglet "Général" du routeur, cochez **Routage automatique** pour que le routeur gère automatiquement les tables de routage (qui sont au programme de Terminale).

<img class="centre image-responsive" src="data/filius_routeur.png" alt="les deux réseaux reliés par un routeur">

Comme le routeur est une machine qui appartient aux deux réseaux R1 et R2, il faut configurer les deux interfaces du routeur (avec des adresses IP cohérentes) : celle reliée au switch S1 et celle reliée au switch S2.

💻 **Question 8** : Dans les onglets correspondant aux deux interfaces, indiquez l'adresse IP 192.168.0.254 côté S1 et l'adresse IP 192.168.1.254 côté S2. Ce sont les @IP respectives du routeur dans les réseaux R1 et R2.

## Configuration de la passerelle.

Ce n'est pas encore fini, car il faut maintenant indiquer à chaque machine qu'elle doit envoyer le message à son routeur si le destinataire n'appartient à son propre réseau. Pour cela, on va définir la **passerelle** pour chaque machine.

💻 **Question 9** : Cliquez sur la première machine 192.168.0.1 et indiquez l'adresse IP du routeur sur la ligne "Passerelle" (l'@IP du routeur dans R1). Faites de même avec toutes les autres machines des deux réseaux en veillant à saisir comme passerelle la bonne adresse IP du routeur selon le réseau.

Et voilà, normalement le réseau global est configuré !

💻 **Question 10** : Faites un ping de la machine 192.168.0.1 vers n'importe quelle machine du second réseau. Vous devriez constater que les messages parviennent bien à destination ! (cela marche aussi évidemment entre machines du même réseau).

---

**Références** :
- Pour l'idée du TP : activité d'Olivier Lecluse (https://www.lecluse.fr/snt/internet/3_filius/), elle même adaptée d'une activité de David Roche (https://dav74.github.io/site_snt/a4/)

---

Germain Becker, Lycée Emmanuel Mounier, Angers.  
Sous licence [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.fr).

![Licence Creative Commons](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)