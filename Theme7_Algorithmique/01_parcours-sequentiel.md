Algorithmes de parcours séquentiel d'un tableau
===============================================

Les algorithmes de parcours séquentiel permettent de parcourir les éléments d'un tableau à l'aide d'une boucle.

Ils font partie des algorithmes fondamentaux en informatique, leur parfaite maîtrise est donc indispensable car ils sont à la base de beaucoup d'autres algorithmes qui doivent passer en revue tous les éléments d'une collection (tableau, tuple, liste, dictionnaires, etc.).

On détaille dans ce chapitre les trois algorithmes au programme :

* l'algorithme de recherche d'un élément (ou d'une occurrence)
* l'algorithme de recherche d'un extremum (maximum ou minimum)
* l'algorithme de calcul d'une moyenne

# Algorithme de recherche d'un élément

On veut écrire un algorithme qui cherche si un élément est présent ou non dans un tableau.

<img class="centre image-responsive" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPHN2ZyB2aWV3Qm94PSIwIDAgMjU2IDI1NiIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KIDxkZWZzPgogIDxmaWx0ZXIgaWQ9ImkiPgogICA8ZmVHYXVzc2lhbkJsdXIgc3RkRGV2aWF0aW9uPSIxLjQ0OTAyNSIvPgogIDwvZmlsdGVyPgogIDxmaWx0ZXIgaWQ9ImgiPgogICA8ZmVHYXVzc2lhbkJsdXIgc3RkRGV2aWF0aW9uPSIwLjc3NTAxOSIvPgogIDwvZmlsdGVyPgogIDxsaW5lYXJHcmFkaWVudCBpZD0ibSIgeDE9Ii44MzcxMSIgeDI9Ii4wNzI3NjEiIHkxPSIuMTY4MTQiIHkyPSIuNzgxODQiPgogICA8c3RvcCBzdG9wLWNvbG9yPSIjZmZmIiBvZmZzZXQ9IjAiLz4KICAgPHN0b3Agc3RvcC1jb2xvcj0iI2ZmZiIgc3RvcC1vcGFjaXR5PSIwIiBvZmZzZXQ9IjEiLz4KICA8L2xpbmVhckdyYWRpZW50PgogIDxsaW5lYXJHcmFkaWVudCBpZD0ibCIgeDE9Ii4yNDE5MyIgeDI9Ii41NzU0NiIgeTE9Ii43MDk0NSIgeTI9Ii4zNTQxOCI+CiAgIDxzdG9wIHN0b3AtY29sb3I9IiNmZmYiIG9mZnNldD0iMCIvPgogICA8c3RvcCBzdG9wLWNvbG9yPSIjZmZmIiBzdG9wLW9wYWNpdHk9IjAiIG9mZnNldD0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJrIiB4MT0iLjE3OTQ5IiB4Mj0iMS4wNTQ0IiB5MT0iLjg1MzMzIj4KICAgPHN0b3Agc3RvcC1jb2xvcj0iIzk5OSIgb2Zmc2V0PSIwIi8+CiAgIDxzdG9wIHN0b3AtY29sb3I9IiNmMmYyZjIiIG9mZnNldD0iLjU1NDY5Ii8+CiAgIDxzdG9wIHN0b3AtY29sb3I9IiNiM2IzYjMiIG9mZnNldD0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJqIiB4MT0iLjM5Njc2IiB4Mj0iLjY2NzQ2IiB5MT0iLjYyMzgiIHkyPSIuMzgxNjQiPgogICA8c3RvcCBzdG9wLWNvbG9yPSIjMWMzZTQ3IiBvZmZzZXQ9IjAiLz4KICAgPHN0b3Agc3RvcC1jb2xvcj0iIzg4YWViOSIgb2Zmc2V0PSIuNDI3NjUiLz4KICAgPHN0b3Agc3RvcC1jb2xvcj0iI2I5ZDBkNiIgb2Zmc2V0PSIuNTg0NSIvPgogICA8c3RvcCBzdG9wLWNvbG9yPSIjODhhZWI5IiBvZmZzZXQ9Ii43NDQ4NiIvPgogICA8c3RvcCBzdG9wLWNvbG9yPSIjMmQ1MDU5IiBvZmZzZXQ9IjEiLz4KICA8L2xpbmVhckdyYWRpZW50PgogIDxyYWRpYWxHcmFkaWVudCBpZD0ibiIgY3g9IjEyMy4xNyIgY3k9IjcuOTY1OCIgcj0iNzcuNSIgZ3JhZGllbnRUcmFuc2Zvcm09Im1hdHJpeCgtMS41Mzc2IC0yLjY2MzIgMi42NjU4IC0xLjUzOTEgMzE1Ljc3IDQ5NC45OSkiIGdyYWRpZW50VW5pdHM9InVzZXJTcGFjZU9uVXNlIj4KICAgPHN0b3Agb2Zmc2V0PSIwIi8+CiAgIDxzdG9wIHN0b3AtY29sb3I9IiNmZmYiIG9mZnNldD0iMSIvPgogIDwvcmFkaWFsR3JhZGllbnQ+CiA8L2RlZnM+CiA8cGF0aCBkPSJtMTM2Ljk0IDE1Mi42NGw1LjA1MSA1LjMwMyAxNC42NDctMTMuNjM3LTUuMDUtNS4zMDMtMTQuNjQ4IDEzLjYzN3oiIGZpbGw9InVybCgjaykiIGZpbGwtcnVsZT0iZXZlbm9kZCIvPgogPHBhdGggZD0ibTkwLjgxIDEyLjgzYy00MS44NTcgMS4wNjE2LTc1LjUgMzUuMzg4LTc1LjUgNzcuNSAwIDQyLjc4IDM0LjcyIDc3LjUgNzcuNSA3Ny41czc3LjUtMzQuNzIgNzcuNS03Ny41LTM0LjcyLTc3LjUtNzcuNS03Ny41Yy0wLjY2OCAwLTEuMzM1LTAuMDE2ODYtMiAwem0yIDcuNWMzOC42NCAwIDcwIDMxLjM2IDcwIDcwcy0zMS4zNiA3MC03MCA3MC03MC0zMS4zNi03MC03MCAzMS4zNi03MCA3MC03MHoiIGZpbHRlcj0idXJsKCNoKSIvPgogPHBhdGggZD0ibTkwLjgxIDEyLjgzYy00MS44NTcgMS4wNjE2LTc1LjUgMzUuMzg4LTc1LjUgNzcuNSAwIDQyLjc4IDM0LjcyIDc3LjUgNzcuNSA3Ny41czc3LjUtMzQuNzIgNzcuNS03Ny41LTM0LjcyLTc3LjUtNzcuNS03Ny41Yy0wLjY2OCAwLTEuMzM1LTAuMDE2ODYtMiAwem0yIDcuNWMzOC42NCAwIDcwIDMxLjM2IDcwIDcwcy0zMS4zNiA3MC03MCA3MC03MC0zMS4zNi03MC03MCAzMS4zNi03MCA3MC03MHoiIGZpbGw9InVybCgjbikiLz4KIDxwYXRoIGQ9Im0xMTQuNzYgMjYuNTU1Yy0yMy43MTMtOC42MzU2LTYzLjcyOS00LjE2MDgtNzQuNTg2IDI3LjEyOCAxMS43NTEgMC42OTgxIDI4LjM2OCAxMi4yMTMgNDAuMjc2IDE2LjU0OSAzNy43NzYgMTMuNzU2IDY2Ljg4NSAxNy44NCA3NC42NzggNDkuNjc3IDAuNTEwOTktMS4xOTQgMS4wMDQtMi4zOTIgMS40NTQtMy42MjcgMTMuMjIyLTM2LjMwOC01LjUxNC03Ni41MDYtNDEuODIyLTg5LjcyN3oiIGZpbGw9InVybCgjbSkiLz4KIDxwYXRoIGQ9Im0yOC41NiA5Mi4yNjdjMC40Nzg5IDE1LjQ0NiA2LjU1MjEgMzAuMjE4IDE2LjkwNiA0MS41MzJsNi45Mzc1LTcuNDM4Yy0xMC4yMDItOS4xMTEtMTguNjczLTIwLjMxNi0yMy44NDQtMzQuMDk0em0yNS4xMjUgMzUuMjE5bC03IDcuNjI1YzYuNTQ4IDYuNzY0IDE0LjY5IDEyLjE4OSAyNC4xMjUgMTUuNjI1IDEwLjM3OSAzLjc4IDIxLjA5NyA0LjY5NyAzMS4zMTMgMy4xODgtMTYuNjM0LTUuNjg4LTM0LjEzOS0xNC4wODgtNDguNDM4LTI2LjQzOHoiIGZpbGw9InVybCgjbCkiIG9wYWNpdHk9Ii43Ii8+CiA8cGF0aCBkPSJtOTAuOTQxIDE3Ljg4Yy0zOS4xMyAwLjk5MjQ2LTcwLjU4IDMzLjA4Mi03MC41OCA3Mi40NSAwIDM5Ljk5MiAzMi40NTcgNzIuNDQ5IDcyLjQ1IDcyLjQ0OSAzOS45OTIgMCA3Mi40NDktMzIuNDU3IDcyLjQ0OS03Mi40NDlzLTMyLjQ1Ny03Mi40NS03Mi40NDktNzIuNDVjLTAuNjI1IDAtMS4yNDktMC4wMTU3NDktMS44NyAwem0xLjg3IDcuMDExMmMzNi4xMjEgMCA2NS40MzggMjkuMzE2IDY1LjQzOCA2NS40MzhzLTI5LjMxNyA2NS40MzgtNjUuNDM4IDY1LjQzOGMtMzYuMTIyIDAtNjUuNDM5LTI5LjMxNi02NS40MzktNjUuNDM4czI5LjMxNi02NS40MzggNjUuNDM5LTY1LjQzOHoiIGZpbHRlcj0idXJsKCNpKSIgb3BhY2l0eT0iLjIiLz4KIDxwYXRoIGQ9Im0xMzguNTYgMTYwLjU1bDc5LjQ1OSA4Ny45NDRjOC4zMzQtMy4yODMgMTkuODYzLTE1LjAyOSAyMy45MDMtMjIuNjA1bC04MS42MzctODYuMTc2LTIxLjcyNSAyMC44Mzd6IiBmaWxsPSJ1cmwoI2opIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiIHN0cm9rZT0iIzAwMCIgc3Ryb2tlLWxpbmVqb2luPSJiZXZlbCIvPgo8L3N2Zz4K" alt="loupe" width="180">


## Spécification de l'algorithme


<div class="important" markdown="1">

La **spécification** d’un algorithme précise de manière non ambigüe ce que doit faire un algorithme. En particulier, on y indique : le nom des données manipulées (entrées), le nom des données renvoyées (sorties), le rôle de l’algorithme ainsi que les hypothèses sur les entrées (précondition) et les sorties (postcondition). Un algorithme sera considéré comme *correct* s’il respecte cette spécification.

</div>

Voici la spécification de l'algorithme de recherche d'un élément :

- **Entrée(s)** : un tableau `T` de taille $n$ et une valeur `v`
- **Sortie(s)** : un booléen `trouvee`
- **Rôle** : chercher si la valeur `v` est présente dans `T`
- **Précondition(s)** : `v` est du même type que les éléments de `T`
- **Postcondition(s)** : `trouvee` vaut Vrai si `v` appartient à `T`, et Faux sinon

## Algorithme

L'idée est la suivante : 

- le booléen `trouvee` est initialisé à `Faux`
- on parcourt une à une les valeurs du tableau `T` :
    - si on en trouve une égale à `v`, le booléen `trouvee` prend la valeur `Vrai`
    - sinon on ne fait rien

Voici l'algorithme :

```
trouvee ← Faux
pour i de 0 à n-1 faire :
    si T[i] = v alors :
        trouvee ← Vrai
    fin si
fin pour
```

## Coût de l'algorithme

<div class="important" markdown="1">

Le **coût d'un algorithme** est le nombre d’opérations élémentaires (arithmétiques ou logiques) ainsi que d’affectations nécessaires à son exécution **dans le pire cas**.  
Le coût d’un algorithme dépend toujours de la taille des données d’entrée (ici la taille $n$ du tableau).

</div>

Dans tous les cas, notre algorithme va effectuer $n$ tours de boucle, donc dans le pire cas aussi. Ainsi, la comparaison `T[i] = v` sera effectuée $n$ fois. 

Si tous les éléments du tableau sont égaux à `v` alors l'affectation `trouvee ← Vrai` est effectuée à chaque tour de boucle, donc $n$ fois.

On dénombre ainsi :

| Comparaisons | Affectations | Opérations arithmétiques |
| --- | --- | --- |
| $n$ | $n+1$ | $0$ |

Le coût de l'algorithme (dans le pire cas) est donc $n + (n+1) + 0 = 2n + 1$. 

Comme ce coût est de l'ordre de $n$, on dit que le coût de l'algorithme de recherche d'un élément est **linéaire**. 

## Écriture en une fonction Python

On peut écrire une fonction `appartient()` possédant deux paramètres `v` et `T` qui implémente cet algorithme. Cette fonction renvoie `True` si `v` est dans `T` et `False` sinon.


```python
def appartient(v, T):
    trouvee = False
    for i in range(len(T)):
        if T[i] == v:
            trouvee = True
    return trouvee
```

On peut tester cette fonction :


```python
>>> appartient(5, [2, 3, 1, 5, 0])
True
>>> appartient(1, [0, 3, 0, 0])
False
```


### Parcours par valeur

Il est aussi possible de parcourir le tableau *par valeur* et non *par indice* car on n'a pas besoin des indices d'après la spécification de l'algorithme. La fonction suivante implémente cette possibilité :


```python
def appartient_v2(v, T):
    trouvee = False
    for e in T:
        if e == v:
            trouvee = True
    return trouvee
```

On peut tester :


```python
>>> appartient_v2(5, [2, 3, 1, 5, 0])
True
>>> appartient_v2(1, [0, 3, 0, 0])
False
```


### Sortie anticipée

L'algorithme ainsi écrit et implémenté par les deux fonctions précédentes peut sembler non optimal. En effet, si le premier élément du tableau `T` est la valeur `v` alors on sait de suite que la réponse est "Vrai" mais notre algorithme va quand même tester toutes les valeurs suivantes du tableau `T`.

On peut utiliser le mot clé `return` pour stopper l'exécution de notre fonction dès que la valeur `v` est trouvée. Dans le cas, où la boucle `for` a atteint la fin du tableau sans trouver `v`, il faut renvoyer `False`.


```python
def appartient_v3(v, T):
    for e in T:
        if e == v:
            return True  # renvoie True dès qu'on trouve v (ce qui stoppe l'exécution de la fonction)
    return False  # après la boucle (si v n'a pas été trouvé)
```


```python
>>> appartient_v3(5, [2, 3, 1, 5, 0])
True
>>> appartient_v3(1, [0, 3, 0, 0])
False
```


**Remarques** :

1. C'est cette dernière version qui est la plus simple et la plus rapide à écrire. On pourra donc l'utiliser sans problème.
2. Si notre algorithme ne devait plus renvoyer Vrai ou Faux mais l'indice de la valeur `v` cherchée, alors il aurait été obligatoire de parcourir le tableau par indice, pour garder trace des indices et renvoyer celui demandé. Le choix du parcours dépend donc de la spécification de l'algorithme à écrire !
3. On aurait également pu écrire une version optimisée (avec arrêt dès que `v` est trouvée) sans utiliser une sortie anticipée (avec `return`) à condition d'utiliser une boucle `while`. Cependant l'écriture est plus longue et compliquée :

```python
def appartient_v4(v, T):
    i = 0
    trouvee = False
    while i <= len(T) and trouvee == False:
        if T[i] == v:
            trouvee = True
        i = i + 1
    return trouvee
```


# Algorithme de recherche du maximum

On veut écrire un algorithme qui recherche la valeur maximale dans un tableau.

> La recherche du maximum est présentée ici, mais ce qui va être dit est vrai pour la recherche d'un extremum de manière générale, qu'il s'agisse d'un maximum ou d'un minimum).

## Spécification de l'algorithme

- **Entrée(s)** : un tableau `T` de taille $n$
- **Sortie(s)** : un entier `maxi`
- **Rôle** : chercher l'élément maximal de `T`
- **Précondition(s)** : `T` est un tableau *non vide* d'entiers
- **Postcondition(s)** : `maxi` est un entier qui est l'élément maximal de `T`


## Algorithme

L'idée est la suivante : 

- on initialise la valeur maximale par le premier élément du tableau ;
- on parcourt une à une les valeurs du tableau `T` (à partir de la deuxième) :
    - si on en trouve une strictement supérieure au maximum provisoire, cette valeur devient notre nouveau maximum (provisoire).
    - sinon, on ne fait rien

Voici l'algorithme :

```
maxi ← T[0]
pour i de 1 à n-1 faire :
    si T[i] > maxi alors :
        maxi ← T[i]
    fin si
fin pour
```

## Coût de l'algorithme

Dans tous les cas, notre algorithme va effectuer $n-1$ tours de boucle, donc dans le pire cas aussi. Ainsi, la comparaison `T[i] > maxi` sera effectuée $n-1$ fois. 

Si les éléments sont rangés dans l'ordre (strictement) croissant, alors la condition `T[i] > maxi` est vraie à chaque tour de boucle et l'affectation `maxi ← T[i]` est effectuée à chaque tour de boucle.

Le *pire cas* est donc un tableau dans lequel chaque élément est strictement supérieur au précédent (par exemple `[1, 2, 3, 4, 5]` pour un tableau de $n=5$ éléments).

On dénombre ainsi dans le pire cas :

| Comparaisons | Affectations | Opérations arithmétiques |
| --- | --- | --- |
| $n-1$ | $n$ | $0$ |

*Remarque* : on n'a pas compté la soustraction du `n-1` (cela ne change rien au coût)

Le coût de l'algorithme (dans le pire cas) est donc $n-1 + n + 0 = 2n - 1$. 

Comme ce coût est de l'ordre de $n$, on dit que le coût de l'algorithme de recherche d'un extremum est **linéaire**. 

## Écriture en une fonction Python

On peut écrire une fonction `maximum()`, possédant en paramètre un tableau d'entiers `T` non vide, qui implémente cet algorithme. Cette fonction renvoie la valeur maximale présente dans `T`.


```python
def maximum(T):
    maxi = T[0]
    for i in range(1, len(T)):
        if T[i] > maxi:
            maxi = T[i]
    return maxi
```

On peut tester :


```python
>>> maximum([1, 2, 3, 4, 5])
5
>>> maximum([7, 9, 2])
9
```


### Parcours par valeur

Il est aussi possible de parcourir le tableau *par valeur* et non *par indice* car on n'a pas besoin des indices d'après la spécification de l'algorithme. La fonction suivante implémente cette possibilité :


```python
def maximum_v2(T):
    maxi = T[0]
    for e in T:
        if e > maxi:
            maxi = e
    return maxi
```

**Remarques** :

1. Cette version a le (petit) désavantage de parcourir le premier élément du tableau (au premier tour de boucle) alors que ce n'est pas utile, mais cela ne change rien au coût, qui reste linéaire, car cela n'ajoute qu'une comparaison et qu'une affectation (le coût est donc $2n+1$, donc toujours linéaire).
2. Si notre algorithme ne devait plus renvoyer la valeur maximale mais une position de cette valeur maximale, alors il aurait été obligatoire de parcourir le tableau par indice, pour garder trace des indices et renvoyer celui demandé. Le choix du parcours dépend donc de la spécification de l'algorithme à écrire !

# Algorithme de calcul d'une moyenne

On veut écrire un algorithme qui calcule la moyenne des nombres présents dans un tableau.

<img class="centre image-responsive" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPHN2ZyB3aWR0aD0iNTAwIiBoZWlnaHQ9IjUwMCIgdmVyc2lvbj0iMS4xIiB2aWV3Qm94PSIwIDAgNTAwLjAwMDAxIDUwMC4wMDAwMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KIDxkZWZzPgogIDxsaW5lYXJHcmFkaWVudCBpZD0iYSIgeDE9IjI1MCIgeDI9IjI1MCIgeTE9IjQyMCIgeTI9IjgwIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSI+CiAgIDxzdG9wIHN0b3AtY29sb3I9IiMzMTJjNGQiIG9mZnNldD0iMCIvPgogICA8c3RvcCBzdG9wLWNvbG9yPSIjMjgyMjNmIiBvZmZzZXQ9Ii4wOTQ4OTQiLz4KICAgPHN0b3Agc3RvcC1jb2xvcj0iIzJkMjY0NiIgb2Zmc2V0PSIuMjIxMDMiLz4KICAgPHN0b3Agc3RvcC1jb2xvcj0iIzFmMWEzMSIgb2Zmc2V0PSIuODgxNDEiLz4KICAgPHN0b3Agc3RvcC1jb2xvcj0iIzE2MTMyMyIgb2Zmc2V0PSIxIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KIDwvZGVmcz4KIDxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAgLTU1Mi4zNikiPgogIDxyZWN0IHk9IjU1Mi4zNiIgd2lkdGg9IjUwMCIgaGVpZ2h0PSI1MDAiIGZpbGw9IiNmYWZjZjYiLz4KIDwvZz4KIDxyZWN0IHg9IjgwIiB5PSI4MCIgd2lkdGg9IjM0MCIgaGVpZ2h0PSIzNDAiIHJ5PSIyMCIgZmlsbD0idXJsKCNhKSIvPgogPGcgdHJhbnNmb3JtPSJtYXRyaXgoLjg5NSAwIDAgLjg5NSAyNi4yNDkgMjYuMjUxKSIgZmlsbD0ibm9uZSIgc3Ryb2tlPSIjZmZmIiBzdHJva2UtbGluZWNhcD0icm91bmQiIHN0cm9rZS1saW5lam9pbj0icm91bmQiIHN0cm9rZS13aWR0aD0iMTUiPgogIDxwYXRoIGQ9Im0xMjEuNjcgMTIxLjY3IDg2LjY1NSA4Ni42NTUiLz4KICA8cGF0aCBkPSJtMjA4LjMzIDEyMS42Ny04Ni42NTUgODYuNjU1Ii8+CiA8L2c+CiA8ZyB0cmFuc2Zvcm09Im1hdHJpeCguODk1IDAgMCAuODk1IDI2LjI0OSAyNi4yNTEpIj4KICA8cGF0aCBkPSJtMjA4LjMzIDMzNWgtODYuNjU1IiBmaWxsPSJub25lIiBzdHJva2U9IiNmZmYiIHN0cm9rZS1saW5lY2FwPSJyb3VuZCIgc3Ryb2tlLWxpbmVqb2luPSJyb3VuZCIgc3Ryb2tlLXdpZHRoPSIxNSIvPgogIDxjaXJjbGUgY3g9IjE2NSIgY3k9IjMwNSIgcj0iNy41IiBmaWxsPSIjZmZmIi8+CiAgPGNpcmNsZSBjeD0iMTY1IiBjeT0iMzY1IiByPSI3LjUiIGZpbGw9IiNmZmYiLz4KIDwvZz4KIDxwYXRoIGQ9Im0zNjQuODUgMTczLjkzaC03Ny41NTYiIGZpbGw9Im5vbmUiIHN0cm9rZT0iI2ZmZiIgc3Ryb2tlLWxpbmVjYXA9InJvdW5kIiBzdHJva2UtbGluZWpvaW49InJvdW5kIiBzdHJva2Utd2lkdGg9IjEzLjQyNSIvPgogPGcgdHJhbnNmb3JtPSJtYXRyaXgoLjg5NSAwIDAgLjg5NSAyNi4yNDkgMjYuMjUxKSIgZmlsbD0ibm9uZSIgc3Ryb2tlPSIjZmZmIiBzdHJva2UtbGluZWNhcD0icm91bmQiIHN0cm9rZS1saW5lam9pbj0icm91bmQiIHN0cm9rZS13aWR0aD0iMTUiPgogIDxwYXRoIGQ9Im0zNzguMzMgMzM1aC04Ni42NTUiLz4KICA8cGF0aCBkPSJtMzM1IDM3OC4zM3YtODYuNjU1Ii8+CiA8L2c+Cjwvc3ZnPgo=" alt="calcul" width="180">




## Spécification de l'algorithme

- **Entrée(s)** : un tableau `T` de taille $n$
- **Sortie(s)** : un réel `m`
- **Rôle** : calculer la moyenne des éléments de `T`
- **Précondition(s)** : `T` est un tableau *non vide* d'entiers
- **Postcondition(s)** : `m` est la moyenne des éléments de `T`

## Algorithme

L'idée est de calculer la somme des valeurs de `T` puis de calculer la moyenne en divisant la somme par le nombre d'éléments de `T` : 

- on initialise la somme des valeurs à 0 ;
- on parcourt une à une les valeurs du tableau `T` :
    - on ajoute chaque valeur à notre somme
- la réponse est la somme divisée par `n` (le nombre d'éléments de `T`)

Voici l'algorithme :

```
s ← 0
pour i de 0 à n-1 faire :
    s ← s + T[i]
fin pour
m ← s/n
```

## Coût de l'algorithme

Dans tous les cas, notre algorithme va effectuer $n$ tours de boucle, donc dans le pire cas aussi. Ainsi, l'affectation `s ← s + T[i]` est effectuée à chaque tour de boucle.

N'importe quel tableau est donc un *pire cas* pour notre algorithme.

On dénombre ainsi :

| Comparaisons | Affectations | Opérations arithmétiques |
| --- | --- | --- |
| $0$ | $n+2$ | $n+1$ |

*Remarque* : il y a $n$ additions (`s ← s + T[i]`) et *une* division (`s/n`) donc $n+1$ opérations arithmétiques.

Le coût de l'algorithme (dans le pire cas) est donc $0 + (n+2) + (n+1) = 2n + 3$. 

Comme ce coût est de l'ordre de $n$, on dit que le coût de l'algorithme de calcul d'une moyenne est **linéaire**.

## Écriture en une fonction Python

On peut écrire une fonction `moyenne()`, possédant en paramètre un tableau d'entiers `T` non vide, qui implémente cet algorithme. Cette fonction renvoie la moyenne des valeurs présentes dans `T`.


```python
def moyenne(T):
    s = 0
    for i in range(len(T)):
        s = s + T[i]
    return s / len(T)
```

On peut tester :


```python
>>> moyenne([10, 14, 6])
10.0
>>> moyenne([8, 15, 12, 14, 19, 11])
13.166666666666666
```


### Parcours par valeur

Il est aussi possible de parcourir le tableau *par valeur* et non *par indice* car on n'a pas besoin des indices. La fonction suivante implémente cette possibilité :


```python
def moyenne_v2(T):
    s = 0
    for e in T:
        s = s + e
    return s / len(T)
```

**Remarque** : Cette fonction est légèrement plus simple à écrire et peut donc être privilégiée pour la calcul de la moyenne des éléments d'un tableau.

# Bilan

* La **spécification** d'un algorithme définie de manière non ambigüe ce qu'un algorithme doit faire, les données avec lesquelles il travaille (les *entrées*) et les données qu'il renvoie (les *sorties*). 
* Un algorithme de **parcours séquentiel** d'un tableau parcoure les éléments du tableau à l'aide d'une boucle. Ces algorithmes sont à la base de beaucoup d'autres algorithmes.
* Les trois algorithmes de parcours séquentiel étudiés ont tous un **coût linéaire**, c'est-à-dire un coût de l'ordre de la taille $n$ du tableau parcouru.
* Le type de parcours, par indice ou par valeur, dépend de l'algorithme et de l'exercice.
* Nous verrons d'autres algorithmes au cours de l'année, dont certain n'ont pas un coût linéaire.


---
**Références** :

- Documents ressources de l'équipe éducative du DIU EIL, Université de Nantes, Christophe JERMANN et Christophe DECLERCQ.

---

Germain BECKER & Sébastien POINT, Lycée Mounier, ANGERS ![Licence Creative Commons](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
