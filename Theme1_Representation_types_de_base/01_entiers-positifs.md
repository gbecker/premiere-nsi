Représentation binaire des entiers positifs
===========================================

# Introduction : analogie avec la base 10

Nous utilisons tous les jours la base 10 pour compter, c'est la base décimale. Par exemple, le nombre 4097 représente 4 milliers, 0 centaine, 9 dizaines et 7 unités.

$4097 = 4\times 1000 + 0 \times 100 + 9 \times 10 + 7 \times 1$ c'est-à-dire : $4097 = \textbf 4 \times  10^3 + \textbf 0\times  10^2 + \textbf 9 \times 10^1 + \textbf 7 \times 10^0$

On retrouve les chiffres $4$, $0$, $9$ et $7$ dans cette écriture avec des puissances de 10. On peut schématiser cela ainsi :

|Puissances de 10 | ... | $10^6$ |$10^5$|$10^4$|$10^3$|$10^2$|$10^1$|$10^0$|
| --- | --- | --- | --- | --- | --- | --- | --- | --- |
|Chiffres | ... | x | x | x | 4 | 0 | 9 | 7 |

En base 10, chaque chiffre peut prendre 10 valeurs : de $0$ à $9$.

**Comment ajouter 1 en base 10 ?** 

Vous le faites intuitivement, mais analysons ce qui se passe avec les chiffres de la représentation décimale. Il faudra faire exactement la même chose en base 2.

On regarde le chiffre des unités : s'il n'est pas au maximum ($\neq 9$) on lui ajoute $1$ ; s'il est au maximum, on le met à zéro et on regarde si on peut ajouter 1 au chiffre des dizaines, et ainsi de suite.

Voyons quelques exemples pour fixer les idées :

- On veut ajouter $1$ à $27$ : comme le chiffre des unités vaut $7$ ($\neq 9$) on lui ajoute directement $1$. On obtient donc $28$ ;
- On veut ajouter $1$ à $29$ : comme le chiffre des unités est à sa valeur maximale, on le met à $0$ et on regarde le chiffre des dizaines : il n'est pas à sa valeur maximale, on peut donc lui ajouter $1$. On obtient donc $30$.
- On veut ajouter $1$ à $99$ : comme le chiffre des unités vaut $9$ on le met à $0$ et on passe au chiffre des dizaines; celui-ci vaut aussi $9$ donc on le met aussi à $0$ et on passe au chiffre des centaines; celui-ci vaut $0$ donc on peut lui ajouter $1$. On obtient donc $100$.

# Codage en base 2

Toute machine informatique, en raison de son architecture (cf. Thème 5, chapitres 1 et 2), ne manipule que des données exprimées en _langage binaire_, dans lequel tous les "mots" s'écrivent uniquement avec des 0 et des 1.

<div class="important" markdown="1">

**Définitions**
- L'unité de base du langage binaire s'appelle le **bit** (contraction de **bi**nary digi**t**, soit *chiffre binaire* en anglais). Un bit ne peut donc prendre que deux valeurs distinctes, notées 0 et 1.
- Une suite de 8 bits est appelée un **octet** (exemple : $10010101$ est un octet)

</div>

Dans la suite, on va voir comment on peut représenter un entier positif uniquement avec des 0 et des 1.

<div class="a-faire titre" markdown="1">

### ✏️ Exercice 1

1. Combien de valeurs peut-on coder sur 1 bit ?
2. Combien de valeurs peut-on coder sur 2 bits ?
3. Combien de valeurs peut-on coder sur 3 bits ?
4. Combien de valeurs peut-on coder sur $n$ bits ?

</div>

<div class="a-faire titre" markdown="1">

### ✏️ Exercice 2 :

1. Quelle est la valeur maximale que l'on peut coder sur 8 bits ?
2. Complétez le tableau ci-dessous avec l'écriture binaire de chaque entier.  
Pour cela, il suffit d'ajouter $1$ exactement de la même manière que ce qui a été vu un peu plus haut pour la base 10.

</div>

| Nombre décimal | Nombre en binaire | Explication
| --- | --- | :--- |
| 0 | 0 | 0 se code 0 en binaire |
| 1 | 1 | on a ajouté 1 au premier bit |
| 2 | 10  | le premier bit a atteint sa valeur maximale : on le passe à 0 et on ajoute 1 au bit suivant |
| 3 | 11  | on a ajouté 1 au premier bit |
| 4 | 100  | les premier et second bits ont atteint leur valeur maximale : on les passe à 0 et on ajoute 1 au bit suivant |
| 5 | ...  | |
| 6 |   | |
| 7 |   | |
| 8 |   | |
| 9 |   | |
| 10 |   | |


> Tout cela est un peu pénible, et heureusement que je ne vous ai pas demandé de convertir 2022 en binaire... Il y a bien entendu une méthode pour convertir un nombre décimal en binaire.

## Algorithme de conversion en binaire par suite de divisions

En divisant le nombre par 2, on obtient comme premier reste, le nombre d'unités. Si on recommence, on obtient les chiffres suivants de l'écriture en base 2. On s'arrête lorsque le quotient vaut 0.

### Exemple : conversion de 42 en base 2

**Méthode 1 : divisions successives par 2 (en ligne)**

$$\begin{aligned}
42 &= 2\times 21 + {\color{red} \mathbf{0}} \newline
21 &= 2 \times 10 + {\color{red} \mathbf{1}} \newline
10 &= 2 \times 5 + {\color{red} \mathbf{1}} \newline
5  &= 2 \times 2 + {\color{red} \mathbf{0}} \newline
2  &= 2 \times 1 + {\color{red} \mathbf{1}} \newline
1 &= 2 \times 0 + {\color{red} \mathbf{0}}
\end{aligned}$$

On obtient la valeur binaire en concaténant les restes obtenus **du dernier au premier** : $42 = (101010)_2$


**Méthode 2 : divisions successives par 2 (divisions posées)**

<img class="image-responsive centre" alt="exemple de divisions" src="data/ex_dec_to_bin.png" width="250">

On obtient la valeur binaire en concaténant les restes obtenus **du dernier au premier** : $42 = (101010)_2$

**Méthode 3 : Somme de puissances de 2**

Les puissances de 2 sont : 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, etc. On peut trouver la plus grande puissance de 2 inférieure ou égale au nombre décimal à convertir. La plus grande puissance de 2 inférieure ou égale à 42 est 32. Il manque alors 10 pour faire 42, donc on recommence avec 10 en cherchant la plus grande puissance de 2 inférieure ou égale à 10, etc :

$$\begin{align}
42 &= 32 + (10) \newline
   &= 32 + 8 + (2) \newline
   &= 32 + 8 + 2 \newline
   &= 2^5 + 2^3 + 2^1 \newline
   &= \mathbf{\color{red} 1} \times 2^5 + \mathbf{\color{red} 0} \times 2^4 + \mathbf{\color{red} 1} \times 2^3 + \mathbf{\color{red} 0} \times 2^2 + \mathbf{\color{red} 1} \times 2^1 + \mathbf{\color{red} 0} \times 2^0
\end{align}$$

Donc $42 = (101010)_2$.

**En Python**

On peut calculer l'écriture en binaire d'un entier avec la fonction Python `bin()`.

```python
>>> bin(42)
'0b101010'
```

<div class="a-faire titre" markdown="1">

### ✏️ Exercice 3 :

Convertissez (à la main) en base 2 les nombres décimaux suivants : 63, 257, 2023.  
*Vous pourrez vérifier vos résultats avec Python.*

</div>

## Conversion binaire → décimale

La conversion binaire vers décimale est plus simple.

De la même manière que les chiffres de l'écriture décimale représentent des puissances de 10, les chiffres de l'écriture binaire représentent des puissances de 2. 

Prenons l'exemple du nombre binaire $11010111$ :

|Puissances de 2 | $2^7$ | $2^6$ | $2^5$ | $2^4$ | $2^3$ | $2^2$ | $2^1$ | $2^0$ |
| ---| --- | --- | --- | --- | --- | --- | --- | --- |
|Chiffres | 1 | 1 | 0 | 1 | 0 | 1 | 1 | 1 |

Ainsi, pour convertir $(11010111)_2$ en décimal, on multiplie le premier chiffre (**celui de droite**) par $2^0$, le second par $2^1$, le troisième par $2^2$, etc. On calcule ensuite la somme de ces valeurs pour obtenir le nombre en décimal.

On obtient :
$$(11010111)_2 = \textbf 1 \times 2^0 + \textbf 1 \times 2^1 + \textbf 1 \times 2^2 + \textbf 0 \times 2^3 + \textbf 1 \times 2^4 + \textbf 0 \times 2^5 + \textbf 1 \times 2^6 + \textbf 1 \times 2^7$$
c'est-à-dire :
$$(11010111)_2 = 1 + 2 + 4 + 0 + 16 + 0 + 64 + 128 = 215.$$

<blockquote class="attention">
    <p>Attention à bien commencer par le chiffre des unités !</p>
</blockquote>

**En Python**

On peut utiliser la fonction `int()` pour convertir un nombre binaire en décimal :


```python
>>> int('11010111', 2)  # le '2' pour indiquer que la chaine est en base 2
215
```

On peut aussi directement saisir un nombre en binaire en préfixant son écriture par `0b`.


```python
>>> 0b11010111
215
```

<div class="a-faire titre" markdown="1">

### ✏️ Exercice 4 :

Convertissez en décimal les nombres binaires suivants : $(11011)_2$ et $(1100011011)_2$.  
*Vous pourrez vérifier vos résultats avec Python.*

</div>

> Dans les exercices, on écrira les algorithmes de conversion entre base 10 et base 2. On verra également comment additionner ou multiplier deux nombres binaires. 

# Codage en base 16

Cette base est appelée **base hexadécimale** (hexa = 6 + décimale = 10).
Nous allons passer plus rapidement cette partie car les méthodes restent les mêmes qu'avec la base 2.

- En base 10 : chaque chiffre peut être codé par 10 valeurs ($0,1,2,3,4,5,6,7,8,9$).
- En base 2 : chaque chiffre peut être codé par 2 valeurs ($0,1$).
- En base 16 : sans surprise, chaque chiffre peut être codé par 16 valeurs ($0,1,2,3,4,5,6,7,8,9,...$ et après ?). Après le $9$, on va utiliser les lettres de l'alphabet pour arriver à 16 valeurs en tout.

Ainsi, en base 16, chaque "chiffre" est codé par : $0, 1, 2, 3, 4, 5, 6, 7, 8, 9, A, B, C, D, E, F$ avec la correspondance suivante :

| Base 10 | Base 16 | Base 2 |
| --- | --- | --- |
| 0 | 0 | 0 |
| 1 | 1 | 1 |
| 2 | 2 | 10 |
| 3 | 3 | 11 |
| 4 | 4 | 100 |
| 5 | 5 | 101 |
| 6 | 6 | 110 |
| 7 | 7 | 111 |
| 8 | 8 | 1000 |
| 9 | 9 | 1001 |
| 10 | A | 1010 |
| 11 | B | 1011 |
| 12 | C | 1100 |
| 13 | D | 1101 |
| 14 | E | 1110 |
| 15 | F | 1111 |

## Pourquoi la base 16 ?

L'avantage de la base 16 réside dans sa facilité de conversion de et vers la base 2. Un chiffre en base 16 remplace 4 bits (chiffres en base 2). Elle est donc un compromis entre le code binaire des machines et une base de numération pratique à utiliser pour les ingénieurs.

<blockquote class="information" markdown="1">

L'hexadécimal a été utilisé la première fois en 1956 par les ingénieurs de l'ordinateur Bendix G-15. Pour plus d'informations sur le système hexadécimal : [Système hexadécimal - Wikipédia](https://fr.wikipedia.org/wiki/Syst%C3%A8me_hexad%C3%A9cimal).
</blockquote>

## Conversion en hexadécimal

L'algorithme est exactement le même que pour la conversion en binaire. Il suffit juste de diviser successivement par 16 et non par 2.

### Exemple : conversion de 215 en base 16

**Méthode 1**

215 = 16 $\times$ 13 + <span style="color:red; font-weight:bold">7</span>   
13 = 16 $\times$ 0 + <span style="color:red; font-weight:bold">13 (→ D)</span>

On obtient la valeur hexadécimale en concaténant les restes obtenus **du dernier au premier** : $215 = (D7)_{16}$


**Méthode 2**

<img class="centre image-responsive" alt="exemple de divisions" src="data/ex_dec_to_hex.png" width="130">

On obtient la valeur hexadécimale en concaténant les restes obtenus **du dernier au premier** : $215 = (D7)_{16}$

> La troisième méthode est possible mais est beaucoup moins adaptée car il faut connaître les puissances de 16.

**En Python**

On peut calculer l'écriture en binaire d'un entier avec la fonction Python `hex()`.


```python
>>> hex(215)
'0xd7'
```

**Pourquoi la base 16 ? (bis)**

<!-- Ci-dessous des div pour les problèmes de rendu latex avec mathjax -->
<div>

Grâce au tableau ci-dessus, on sait que $(D)_{16}=(1101)_2$ et que $(7)_{16}=(0111)_2$ donc on peut convertir très rapidement $(D7)_{16}$ en base 2 : $(11010111)_2$.

</div>


<div>

Réciproquement si on connait un nombre binaire $(11000011)_2$, on convertit les bits par blocs de 4 : $(1100)_2=(C)_{16}$ et $(0011)_2=(3)_{16}$ et on a très rapidement : $(11000011)_2 = (C3)_{16}$
</div>


## Conversion hexadécimale → décimale

Le principe est le même que la conversion binaire vers décimale.

Plutôt qu'un long discours, un exemple :

**Exemple : conversion de $(4E8)_{16}$ en base 10 :**

|Puissances de 16 | $16^2$ | $16^1$ | $16^0$ |
| ---| --- | --- | --- |
|Chiffres | 4 | E | 8 |

Donc : $(4E8)_{16}=8\times 16^0 + E\times 16^1 + 4 \times  16^2 = 8 \times 1 + 14 \times 16 + 4 \times 256 = 8 + 224 + 1024 = 1256$.

**En Python**

On peut utiliser la fonction `int()` pour convertir une valeur hexadécimale en décimal :


```python
>>> int('4e8', 16)  # on précise bien que la base d'origine est la base 16
1256
```

On peut aussi directement saisir un nombre en hexadécimal en préfixant son écriture par `0x`.


```python
>>> 0x4e8
1256
```

<div class="a-faire titre" markdown="1">

### ✏️ Exercice 5

1. Convertissez $457$ en base 16.
2. Convertissez $(876)_{16}$ en base 10.

</div>

# Ecriture d'un entier positif dans une base $b\geq 2$ quelconque

## Généralisation 

Dans une base $b\geq 2$ quelconque, on procède exactement de la même façon mais avec des puissances de $b$ !

**Exemple : écriture de l'entier 38 en base 5**

$$38 = \textbf 1 \times 5^2 + \textbf 2 \times 5^1 + \textbf 3 \times 5^0.$$ 
Le nombre 38 s'écrit donc $(123)_5$ en base 5.

**Écriture d’un entier positif dans une base $b \geq 2$ :**

<div>

Soit $x = a_n\times b^n + a_{n-1}\times b^{n-1} + ... + a_2\times b^2 + a_1\times b^1 + a_0\times b^0$,
</div>

où les $a_i$ sont des chiffres entre 0 et $b-1$

<div>

Dans ce cas, $a_n a_{n-1}... a_2 a_1 a_0$ est l'écriture en base $b$ de $x$. 
</div>

On note cela :

<div>

$$ x = (a_n a_{n-1}... a_2 a_1 a_0)_b .$$
</div>

# Bilan

- L'**écriture binaire**, aussi appelée écriture en **base 2**, est similaire à l'écriture décimale (en base 10) que nous utilisons tous les jours, mais les nombre s'écrivent avec seulement 2 chiffres (0 et 1), et on doit raisonner en puissances de 2, en divisant par 2, etc.
- Comme une machine ne manipule que *deux* valeurs : 0 et 1, on dit qu'elle parle le langage binaire. Le **bit** est l'unité de base du langage est les mots de 8 bits sont appelés des **octets**.
- Les entiers (comme tout le reste) sont donc nécessairement écrits en binaire pour être manipulé par un ordinateur. On a vu comment passer de la base 10 à la base 2 (et réciproquement).
- La base **hexadécimale** (base 16) est aussi très utilisée en informatique car c'est un compromis intéressant entre la base 2 (compliquée à utiliser pour un humain) et la base 10 (utilisée facilement). Le passage en base 16 est similaire à celui en base 2, mais en raisonnant avec des puissances de 16, en divisant par 16, etc.
- On peut théoriquement définir une base $b \geq 2$ quelconque mais la maîtrise des bases 2 et 16 (et 10 !) est largement suffisante pour comprendre la manière dont sont représentées les informations en machine (ce que nous poursuivrons dans les chapitres suivants de ce thème).

---
**Références :**

- Équipe pédagogique du DIU EIL, université de Nantes.
- Cour de David Roche : https://pixees.fr/informatiquelycee/n_site/nsi_prem_base_2_16.html

---
Germain BECKER & Sébastien POINT, Lycée Mounier, ANGERS ![Licence Creative Commons](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
