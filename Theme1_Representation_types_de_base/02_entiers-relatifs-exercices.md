Représentation des entiers relatifs - EXERCICES
===============================================

# Exercice 1

1. Représentez l’entier $-6$ en complément à deux sur 4 bits.
2. Représentez l’entier $-6$ en complément à deux sur 8 bits.
3. Représentez l’entier $6$ en complément à deux sur 4 bits.
4. Représentez l’entier $-124$ en complément à deux sur 8 bits.


# Exercice 2

1. Déterminez l’entier représenté par 1110 en complément à deux sur 4 bits ?
2. Déterminez l’entier représenté par 10101110 en complément à deux sur 8 bits ?
3. Déterminez l’entier représenté par 01000011 en complément à deux sur 8 bits ?


# Exercice 3

1. Peut-on coder l’entier $-132$ en complément à deux sur 8 bits ? Expliquez.
2. Quel est le plus petit entier négatif que l’on peut représenter en complément à 2 sur 16 bits ?
3. Quel est le plus grand entier que l’on peut représenter en complément à 2 sur 16 bits ?


# Exercice 4

1. Représentez $-53$ en complément à deux sur 8 bits.
2. Représentez $54$ en complément à deux sur 8 bits.
3. Vérifiez que $54+(-53)$ vaut bien $1$.

---
Germain Becker & Sébastien Point, Lycée Emmanuel Mounier, Angers
![Licence Creative Commons BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)


