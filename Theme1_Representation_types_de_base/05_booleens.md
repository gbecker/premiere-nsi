Booléens : opérateurs et portes logiques
========================================

# Introduction 

En Python, comme dans les autres langages de programmation, une comparaison ou un test d’égalité est une expression qui peut être soit vraie, soit fausse. Les deux résultats possibles, vrai et faux, s’appellent des valeurs **booléennes**.

Ce terme tire son nom du mathématicien **Georges Boole** qui a défini, entre autre, une représentation des opérations logiques à l'aide d'opérations mathématiques. Cette modélisation s'appelle **l'algèbre de Boole** et elle est à la base du fonctionnement de tous les circuits électroniques, et donc des ordinateurs.

<img class="centre image-responsive" src="data/George_Boole_color.jpg" alt="Georges Boole">
<p class="legende"><strong>George Boole (1815-1864)</strong>
<br>Crédit : <a href="https://commons.wikimedia.org/wiki/File:George_Boole_color.jpg">Voir la page pour l'auteur</a>, Domaine public, via Wikimedia Commons</p>

Dans l'algèbre de Boole, il n'existe que deux éléments : 0 et 1. C'est pourquoi les valeurs booléennes sont également notées 0 (pour faux) et 1 (pour vrai) et on nous allons voir que l'on peut effectuer des opérations sur ces booléens. 

Les ordinateurs, qui manipulent uniquement des bits 0 et 1, s'appuient sur la théorie de Boole pour effectuer des opérations logiques grâce à des **opérateurs booléens** qui sont réalisés par des circuits électroniques que l’on appelle des **portes logiques** (ou portes booléennes).

*Nous allons d'abord évoquer les opérateurs booléens qui permettent de faire des opérations logiques sur les booléens, puis nous parlerons des circuits électroniques d'un ordinateur qui réalisent effectivement les opérations correspondantes.*

# Opérateurs booléens

<div class="important">
    <p>Un <strong>opérateur booléen</strong> (on dit aussi <em>fonction booléenne</em>) est une fonction mathématique prenant en entrée un ou plusieurs booléens et donnant en sortie un unique booléen.</p>
</div>

Il existe différents opérateurs booléens mais nous allons nous concentrer principalement sur trois d’entre eux, à partir desquels on peut construire tous les autres : *non*, *ou* et *et*.

## Les opérateurs *non*, *ou* et *et*

Comme les booléens sont en nombre fini, on peut représenter chaque opérateur booléen par ce qu’on appelle une **table de vérité**, qui n’est autre qu’un tableau dans lequel on indique *tous* les cas possibles d’entrée(s) et de sortie(s).

### Opérateur *non* (négation)

C’est un opérateur qui transforme vrai en faux et inversement. Sa table de vérité est donc :

| *a*  | *non a* |
|--- | ----- |
| 0  | 1     |
| 1  | 0     |

### Opérateur *ou* (disjonction)

L'opérateur ***ou*** s'applique à *deux* booléens, notés ici ***a*** et ***b***. On note ***a ou b*** le résultat de l’opération, et ***a ou b*** est vrai si et seulement si ***a*** est vrai ou ***b*** est vrai (ou si les deux sont vrais). Sa table de vérité est donc :

| *a* | *b* | *a ou b*  |
|---|---|---------|
| 0 | 0 | 0       |
| 1 | 0 | 1       |
| 0 | 1 | 1       |
| 1 | 1 | 1       |


### Opérateur *et* (conjonction)

L'opérateur ***et*** s'applique à *deux* booléens ***a*** et ***b***. On note ***a et b*** le résultat de l’opération, et ***a et b*** est vrai si et seulement si ***a*** est vrai et ***b*** est vrai. Sa table de vérité est donc :

| *a* | *b* | *a ou b*  |
|---|---|---------|
| 0 | 0 | 0       |
| 1 | 0 | 0       |
| 0 | 1 | 0       |
| 1 | 1 | 1       |

## Opérateur *xor* (= ou exclusif)

L'opérateur ***xor*** (pour exclusive or) signifie « ou exclusif ». Il possède *deux* booléens ***a*** et ***b*** en entrée et ***a xor b*** est vrai si et seulement si ***a*** est vrai et ***b*** est vrai, mais pas si les deux sont vrais. Sa table de vérité est donc :

| *a* | *b* | *a xor b*  |
|---|---|----------|
| 0 | 0 | 0        |
| 1 | 0 | 1        |
| 0 | 1 | 1        |
| 1 | 1 | 0        |

Cet opérateur est très utilisé en informatique également (par exemple pour chiffrer des messages ou pour déterminer si deux ordinateurs appartiennent au même sous-réseau ; ceci sera abordé en classe de Terminale).

# Expressions booléennes

## Définition

Une **expression booléenne** est une expression faisant intervenir des opérateurs booléens et des booléens.

Par exemple, si *a* et *b* sont des booléens, alors ***non(non a ou non b)*** est une expression booléenne. Si on veut connaître la table de vérité de cette expression booléenne, il suffit de la construire étape par étape :

| *a* | *b* | *non a* | *non b* | *non a ou non b* | *non(non a ou non b)*  |
|---|---|-------|-------|----------------|----------------------|
| 0 | 0 | 1     | 1     | 1              | 0                    |
| 1 | 0 | 0     | 1     | 1              | 0                    |
| 0 | 1 | 1     | 0     | 1              | 0                    |
| 1 | 1 | 0     | 0     | 0              | 1                    |

**Explications** :

- La colonne ***non a*** est complétée en prenant la négation des valeurs de ***a***
- Même principe pour la colonne ***non b***
- La colonne ***non a ou non b*** s’obtient en appliquant l’opérateur « ou » entre ***non a*** et ***non b***.
- Enfin la dernière colonne, celle que l’on souhaite, s’obtient en prenant la négation de la colonne précédente.

On se rend compte que la table de vérité de ***non(non a ou non b)*** est identique à celle de ***a et b***, cela veut dire que : ***non(non a ou non b) = a et b***

En particulier, cela signifie qu’il est possible d’exprimer l’opérateur ***et*** à partir des opérateurs ***non*** et ***ou***; et donc qu’en réalité on peut exprimer tous les opérateurs uniquement à partir des opérateurs ***non*** et ***ou***.

L’opérateur ***xor***, comme tous les autres, peut aussi être construit à partir des trois opérateurs précédents. En effet, on peut montrer que l’on a l’égalité suivante : ***a xor b = (a et non b) ou (non a et b)*** (sera fait en exercices).

## En Python

Les booléens *Vrai* et *Faux* s'expriment respectivement en Python :
- par `True` et `False`
- ou par `1` et `0`

Les trois opérateurs ***non***, ***et*** et ***ou*** peuvent être utilisés en Python, respectivement avec : `not`, `and` et `or`. On peut évaluer sans problème des expressions booléennes en Python :

```python
>>> not True
False
>>> not False
True
>>> not 1 == 0
True
>>> True or False
True
>>> 0 or 0
False
>>> 1 and 1
True
>>> True and False
False
```

On peut bien sûr combiner plusieurs opérateurs :

```python
>>> a = 2
>>> b = 3
>>> a > 0 and a < b
True
>>> (True and False) or (False and False)
False
>>> (a > 0 and not True) or (not True and a < b)
False
>>> (1 or 0) and (0 and 1)
0
```

<span class="emoji">⚠️</span> **Attention** : L'opérateur `and` est prioritaire sur `or`. Ce qui fait qu'il n'est pas nécessaire d'écrire certaines parenthèses même si *cela reste conseillé pour plus de clarté* :

```python
>>> True or True and False  # équivaut à True or (True and False)...
True
>>> (True or True) and False
False
>>> True or (True and False)  # ... comme vous pouvez le constater
True
```

<span class="emoji">⚠️</span> **Attention** : L'opérateur `not` est quant à lui prioritaire sur les autres opérations :

```python
>>> not False and False   # équivaut à (not False) and False ...
False
>>> not (False and False)
True
>>> (not False) and False  # ... comme vous pouvez le constater
False
```

# Transistors et portes logiques

<blockquote class="citation" style="width:fit-content;margin:auto">
    <p><em>Au commencement était le transistor,  
    <br>puis nous créâmes les portes booléennes  
    <br>et, à la fin de la journée, les ordinateurs.</em></p>
</blockquote>

Concrètement, les ordinateurs sont capables d’effectuer des opérations logiques comme toutes celles que nous avons décrites. De plus, ils sont capables d’effectuer des opérations arithmétiques (additions, multiplications, etc.) également en utilisant des opérations logiques sur les bits. Nous allons voir comment dans cette dernière partie.

## Transistors

C’est l’invention du **transistor** qui a permis de construire des circuits logiques qui permettent de réaliser les opérations booléennes décrites plus haut (*non*, *ou* , *et*, etc.). Il a été inventé en 1947 par les américains *John Bardeen*, *William Shockley* et *Walter Brattain*.

<img class="centre image-responsive" src="data/Replica-of-first-transistor.jpg" alt="Réplique du premier transistor" width="300">
<p class="legende"><strong>Réplique du premier transistor inventé en 1947</strong>
<br>Crédit : <a href="https://commons.wikimedia.org/wiki/File:Replica-of-first-transistor.jpg">Federal employee</a>, Domaine public, via Wikimedia Commons</p>

<img class="centre image-responsive" src="data/transistors.jpg" alt="Réplique du premier transistor">
<p class="legende"><strong>Quelques transistors</strong>
<br>Crédit : <a href="https://commons.wikimedia.org/wiki/File:Transistors.agr.jpg">ArnoldReinhold</a>, <a href="http://creativecommons.org/licenses/by-sa/3.0/">CC BY-SA 3.0</a>, via Wikimedia Commons</p>

Les circuits électroniques d’un ordinateur sont en réalité composés d’un assemblage de transistors reliés entre eux. Ces circuits ne manipulent que des chiffres binaires : 0 correspond à une *tension basse* (proche de 0 volt) et 1 correspond à une *tension haute* (dont la valeur dépend du circuit).

<blockquote class="information">
    <p>Aujourd’hui, les transistors sont directement gravés dans des plaques de silicium des circuits intégrés et connectés directement entre eux. Un processeur actuel compte des millions, voire des milliards de transistors.</p>
</blockquote>

Un transistor est un composant électronique très simple qui se comporte comme un interrupteur qui laisse ou non passer le courant électrique. Dans le schéma ci-dessous, on a représenté un transistor relié à un générateur E dont la tension est haute (+ V volts). En réalité, il y a aussi des résistances pour éviter les court-circuits mais elles n'ont pas été représentées pour simplifier.

<img class="centre image-responsive" src="data/NOT_gate_circuit_using_transistor.svg" alt="schéma d'un transistor" width="300">
<p class="legende">
  <strong>Schéma d'un transistor</strong>
  <br>Crédit : Modification personnelle du fichier créé par <a href="https://commons.wikimedia.org/wiki/File:Simplified_NOT_gate_circuit_using_transistor.svg">Pradana Aumars</a>, CC0, via Wikimedia Commons
</p>

L'interrupteur est commandé par la broche A :

- si on applique une tension haute à l'entrée A, alors le transistor laisse passer le courant (l'interrupteur est fermé) et la sortie S est reliée à la masse, donc sa tension est basse (voir schéma 1 ci-dessous)
- en revanche, si on applique une tension basse à l'entrée A, alors le transistor bloque le courant (l'interrupteur est ouvert) et la sortie S est reliée à E (sous tension haute), donc elle reste sous tension haute (voir schéma 2 ci-dessous)

| Si A = 1 | Si A = 0 |
| --- | --- |
| ![](data/transistor_ferme.svg) | ![](data/transistor_ouvert.svg)|



## Portes logiques

En connectant de la bonne manière des transistors entre eux, on obtient des *circuits électroniques* qui permettent à une machine de réaliser concrètement les opérateurs booléens vus précédemment (*non*, *ou*, *et*, ...). Ces circuits électroniques classiques s’appellent des **portes logiques**.

Chaque porte logique possède la même table de vérité que l’opérateur booléen qu’elle représente. On donne ci-dessous les symboles américains et européens des portes logiques (même si ce sont surtout les symboles américains qui sont utilisés).

### Porte NON

Une **porte logique NON** est un circuit électronique dont la valeur en sortie est l'inverse de celle de son entrée. Cette porte peut être réalisée avec un simple transistor. En effet, comme nous l'avons vu avec le circuit précédent : si l'entrée A vaut 1 alors la sortie S vaut 0, et si l'entrée vaut 0 alors la sortie est égale à 1. Cela donne donc bien une porte NON.

| Symbole américain | Symbole européen |
| --- | --- |
| <img alt="symbole américain" src="data/porte_non.svg" width="140"> | ![symbole européen](data/porte_non_iec.svg) |

*Vérifiez, en cliquant sur l'entrée A, que la sortie correspond bien à la table de vérité de l'opérateur **non**.* 

<div class="logic-editor centre" style="height: 130px;">
  <logic-editor mode="connect">
    <script type="application/json">
      { // JSON5
        v: 6,
        components: {
          in0: {type: 'in', pos: [85, 55], id: 0},
          not0: {type: 'not', pos: [155, 55], in: 1, out: 2},
          out0: {type: 'out', pos: [225, 55], id: 3},
          label0: {type: 'label', pos: [155, 95], text: 'NON'},
          label1: {type: 'label', pos: [55, 55], text: 'A'},
        },
        wires: [[0, 1], [2, 3]]
      }
    </script>
  </logic-editor>
</div>

### Porte OU

Une **porte logique OU** est un circuit électronique qui réalise l'opérateur ***ou*** : autrement dit un circuit possédant deux entrées A et B (deux tensions, chacune étant soit haute soit basse) et dont la tension en sortie est égale à ***A ou B***.

<details markdown="1" class="effacer-impression en-savoir-plus">
    <summary>Pour voir un circuit d'une porte OU avec trois transistors</summary>

Le circuit ci-dessous, qui possède deux entrées A et B, et une sortie S, est une porte OU (les résistances n'ont pas été représentées) :

<img class="centre image-responsive" src="data/OR_gate_circuit_using_transistors.svg" alt="une porte OU avec trois transistors" width="400">

La sortie S vaut 0 si elle est reliée à la masse donc si transistor de droite est fermé, ce qui est le cas lorsque la valeur de C vaut 1. Pour que la valeur de C soit égale à 1, il faut que C ne soit pas relié à la masse donc que les transistors pilotés par les entrées A et B soient tous les deux ouverts, et donc que A et B valent tous les deux 0. Dans les autres cas, C est relié à la masse donc le transistor de droite est ouvert, ce qui implique que S reste sous tension haute.

En résumé :
- si A et B valent 0 tous les deux, alors S vaut 0 ;</li>
- dans les trois autres cas, S vaut 1.</li>

Cela correspond donc bien à l'opérateur ***ou*** et donc le circuit est une **porte OU**.

Pour simplifier les schémas, on utilisera plutôt le symbole

<img class="centre image-responsive" alt="symbole américain" src="data/porte_ou.svg" width="140">

qui n'est autre qu'une *boîte* contenant un circuit équivalent à celui proposé ci-dessus.

</details>


| Symbole américain | Symbole européen |
| --- | --- |
| <img alt="symbole américain" src="data/porte_ou.svg" width="140"> | ![symbole européen](data/porte_ou_iec.svg) |

*Vérifiez, en cliquant sur les entrées A et B, que la sortie correspond bien à la table de vérité de l'opérateur **ou**.*

<div class="logic-editor centre" style="height: 130px;">
  <logic-editor mode="connect">
    <script type="application/json">
      { // JSON5
        v: 6,
        components: {
          in0: {type: 'in', pos: [60, 35], id: 0},
          in1: {type: 'in', pos: [60, 85], id: 4},
          out0: {type: 'out', pos: [235, 60], id: 5},
          or0: {type: 'or', pos: [165, 60], in: [1, 2], out: 3},
          label0: {type: 'label', pos: [165, 100], text: 'OU'},
          label1: {type: 'label', pos: [25, 35], text: 'A'},
          label2: {type: 'label', pos: [25, 85], text: 'B'},
          label3: {type: 'label', pos: [285, 60], text: 'sortie'},
        },
        wires: [[3, 5], [0, 1], [4, 2]]
      }
    </script>
  </logic-editor>
</div>


### Porte ET

Une **porte logique ET** est un circuit électronique qui réalise l'opérateur ***et*** : autrement dit un circuit possédant deux entrées A et B (deux tensions, chacune étant soit haute soit basse) et dont la tension en sortie est égale à ***A et B***.

<blockquote class="information">
  <p>La porte ET est un circuit électronique qui peut être construit à partir des portes OU et NON (sera fait en exercice).</p>
</blockquote>

| Symbole américain | Symbole européen |
| --- | --- |
| <img alt="symbole américain" src="data/porte_et.svg" width="140"> | ![symbole européen](data/porte_et_iec.svg) |

*Vérifiez, en cliquant sur les entrées A et B, que la sortie correspond bien à la table de vérité de l'opérateur **et**.*

<div class="logic-editor centre" style="height: 130px;">
  <logic-editor mode="connect">
    <script type="application/json">
      { // JSON5
        v: 6,
        components: {
          in0: {type: 'in', pos: [60, 35], id: 0},
          in1: {type: 'in', pos: [60, 85], id: 4},
          out0: {type: 'out', pos: [235, 60], id: 5},
          and0: {type: 'and', pos: [165, 60], in: [6, 7], out: 8},
          label0: {type: 'label', pos: [165, 100], text: 'ET'},
          label1: {type: 'label', pos: [25, 35], text: 'A'},
          label2: {type: 'label', pos: [25, 85], text: 'B'},
          label3: {type: 'label', pos: [285, 60], text: 'sortie'},
        },
        wires: [[8, 5], [0, 6], [4, 7]]
      }
    </script>
  </logic-editor>
</div>

### Porte XOR

Une **porte logique XOR** est un circuit électronique qui réalise l'opérateur ***xor*** : autrement dit un circuit possédant deux entrées A et B (deux tensions, chacune étant soit haute soit basse) et dont la tension en sortie est égale à ***A xor B***.

<blockquote class="information">
  <p>La porte XOR est un circuit électronique qui peut être construit à partir des portes ET, OU et NON (sera fait en exercice).</p>
</blockquote>

| Symbole américain | Symbole européen |
| --- | --- |
| <img alt="symbole américain" src="data/porte_xor.svg" width="140"> | ![symbole européen](data/porte_xor_iec.svg) |

*Vérifiez, en cliquant sur les entrées A et B, que la sortie correspond bien à la table de vérité de l'opérateur **xor**.*

<div class="logic-editor centre" style="height: 130px;">
  <logic-editor mode="connect">
    <script type="application/json">
      { // JSON5
        v: 6,
        components: {
          in0: {type: 'in', pos: [60, 35], id: 0},
          in1: {type: 'in', pos: [60, 85], id: 4},
          out0: {type: 'out', pos: [235, 60], id: 5},
          xor0: {type: 'xor', pos: [165, 60], in: [6, 7], out: 8},
          label0: {type: 'label', pos: [165, 100], text: 'XOR'},
          label1: {type: 'label', pos: [25, 35], text: 'A'},
          label2: {type: 'label', pos: [25, 85], text: 'B'},
          label3: {type: 'label', pos: [285, 60], text: 'sortie'},
        },
        wires: [[8, 5], [0, 6], [4, 7]]
      }
    </script>
  </logic-editor>
</div>

## Addition binaire

Enfin, pour terminer nous allons voir qu’en combinant les portes logiques de la bonne façon on peut créer un circuit électronique qui permet de faire des additions.

<blockquote class="information">
    <p>En suivant le même principe, on peut créer d'autres circuits capables de soustraire, de multiplier, de diviser des nombres.</p>
</blockquote>

### Demi-additionneur (1 bit)

Le circuit suivant, composé d'une porte **ET** et d'une porte **XOR**, est un *demi-additionneur* permettant d'additionner deux bits.

<img class="centre image-responsive" alt="demi additionneur" src="data/Half-adder.svg">
<p class="legende">
  <strong>Demi-additionneur (1 bit)</strong>
  <br>Crédit : <a href="https://commons.wikimedia.org/wiki/File:Half-adder.svg">Cburnett</a>, <a href="http://creativecommons.org/licenses/by-sa/3.0/">CC BY-SA 3.0</a>, via Wikimedia Commons
</p>

Il prend deux bits A et B en entrée, et possède deux sorties :
- S qui est égale à la somme de A et B (on a ***S = A xor B***)
- C qui est la retenue (*carry*, en anglais) de cette somme (on a ***C = A et B***)

Sa table de vérité est la suivante :

| A   | B   | S   | C   |
| --- | --- | --- | --- |
| 0   | 0   | 0   | 0   |
| 0   | 1   | 1   | 0   |
| 1   | 0   | 1   | 0   |
| 1   | 1   | 0   | 1   |

Cela correspond bien à l'addition car :
- $(0)_2 + (0)_2 = (0)_2$ et il n'y a pas de retenue
- $(0)_2 + (1)_2 = (1)_2 + (0)_2 = (1)_2$ et il n'y a pas de retenue
- $(1)_2 + (1)_2 = (10)_2$ donc la somme (sur 1 bit !) fait $0$ et on retient $1$

<div class="a-faire" markdown="1">

**À faire**

Réalisez le circuit du demi-additionneur avec le simulateur ci-dessous, puis vérifiez la table de vérité donné ci-dessus en modifiant les valeurs des deux entrées. Pour ajouter les fils, il suffit de laisser cliquer d'une broche à une autre.

</div>

<div class="logic-editor centre" style="height: 400px;">
  <logic-editor mode="design" showonly="xor and in out">
    <script type="application/json">
      { // JSON5
        v: 6
      }
    </script>
  </logic-editor>
</div>

### Additionneur complet

Pour obtenir un additionneur (sur 1 bit) complet, il faut une entrée supplémentaire : la retenue entrante (notée $\text{C}_\text{in}$ par la suite).

Ce circuit logique, dont le schéma est donné ci-dessous, possède :
- trois entrées : les deux bits A et B ainsi que la retenue entrante $\text{C}_\text{in}$ à additionner (celle résultant de la somme des deux bits précédents); 
- et deux sorties : la valeur S de la somme (sur 1 bit) et la retenue sortante $\text{C}_\text{out}$ (qui servira au calcul de la somme des deux bits suivants)

<img class="centre image-responsive" alt="demi additionneur" src="data/Full-adder.svg">
<p class="legende">
  <strong>Additionneur complet (1 bit)</strong>
  <br>Crédit : <a href="https://commons.wikimedia.org/wiki/File:Full-adder.svg">Cburnett</a>, <a href="http://creativecommons.org/licenses/by-sa/3.0/">CC BY-SA 3.0</a>, via Wikimedia Commons
</p>

<div class="a-faire" markdown="1">

**À faire**

En utilisant le schéma de l'additionneur et les tables de vérité des portes logiques OU, ET et XOR, complétez la table de vérité de l'additionneur ci-dessous. 

*Vous pouvez vérifier avec le simulateur de circuits en-dessous.*

</div>


| A | B | $\text{C}_\text{in}$ | S | $\text{C}_\text{out}$ |
|---|---|----------------------|---|-----------------------|
| 0 | 0 | 0                    | 0 | 0                     |
| 0 | 0 | 1                    | 1 | 0                     |
| 0 | 1 | 0                    |   |                       |
| 0 | 1 | 1                    |   |                       |
| 1 | 0 | 0                    |   |                       |
| 1 | 0 | 1                    |   |                       |
| 1 | 1 | 0                    |   |                       |
| 1 | 1 | 1                    |   |                       |

<p class="legende"><strong>Table de vérité de l'additionneur</strong></p>

<div class="logic-editor centre" style="height: 322px;">
  <logic-editor mode="connect">
    <script type="application/json">
      { // JSON5
        v: 6,
        opts: {showGateTypes: true, wireStyle: 'straight'},
        components: {
          xor0: {type: 'xor', pos: [230, 90], in: [0, 1], out: 2},
          xor1: {type: 'xor', pos: [365, 100], in: [3, 4], out: 5},
          and0: {type: 'and', pos: [365, 190], in: [6, 7], out: 8},
          and1: {type: 'and', pos: [365, 250], in: [9, 10], out: 11},
          or0: {type: 'or', pos: [490, 220], in: [12, 13], out: 14},
          in0: {type: 'in', pos: [90, 65], id: 15, name: 'A'},
          in1: {type: 'in', pos: [90, 115], id: 17, name: 'B'},
          in2: {type: 'in', pos: [90, 155], id: 18, name: 'Cin'},
          out0: {type: 'out', pos: [560, 100], id: 19, name: 'S'},
          out1: {type: 'out', pos: [560, 220], id: 20, name: 'Cout'},
        },
        wires: [[14, 20], [15, 0, {via: [[175, 65], [175, 80]]}], [17, 1, {via: [[175, 115], [175, 100]]}], [2, 3], [18, 4, {via: [[290, 155], [290, 110]]}], [2, 7, {via: [[270, 200, 's']]}], [18, 6, {via: [[290, 155], [290, 180]]}], [8, 12, {via: [[405, 210]]}], [11, 13, {via: [[405, 230]]}], [5, 19], [15, 10, {via: [[135, 65, 's'], [135, 65, 's'], [135, 260, 's']]}], [17, 9, {via: [[155, 115, 's'], [155, 240]]}]]
      }
    </script>
  </logic-editor>
</div>

<p class="legende">Réalisé avec le simulateur <em>logic.modulo-info.ch</em>.
<br><a href="https://logic.modulo-info.ch/?mode=connect&data=N4NwXAbANA9gDgFwM5mEgFjA7gcQIYICmAKgJ5yEoIBOAroVFgJbWEDKCpANoWAORIaeJgHN0CPgF8oAYxgBbODAB2hZclQAPGNQAMqThX7bqfKEpQBtAEwBmXVACcugLpQmysJYcBGNzFoEMGtpEx8Dcl4+EzMLL1sIAFYoH11Xd09LWygAFn9AsETpPGUAE31gQyiS0tiYKwTkn2c3Dy9oAHZ8oIAOYrLwysj+GrqGpKhrRPS2y0cU9ICgnx9pHQqq-h0xrxznSesZzJ9rFNtusB8c6Q8N4b4PHbmHJNbSy+TlPHkogEEpDKDTYPZRPfYrRJvS4dKBfH78ABCAI81giRhBYN8iUh7nePh6sO+UQAwo81oE7uilk9EhBfGkoc1CfC+GwAUsgfdqeZ6l5aQ5rIcoYdmSTqZJpMxWFZLFdJulZckHKAmHgvLKOslXlANckeq4XJI3BqUlAVWrLLqUj4cVbUgajTrTucdfjcmaQKr1dZwdjjT7fKkXIb-VAYebvR0BWkoAI+MHHbKCdAI5aAyk-U7wfqE8aCScPV7LTldMlrEGQ66fGdCxbLCWy-Zczqmo5jTaFrX1T5bFrknH272oBMB66h9Y6bGkPHKyb5qnFU0O6PF5MS7nDUA" target="_blank">Ouvrir ce circuit avec le simulateur</a></p>

En combinant des circuits additionneurs 1 bit de ce type, on peut créer des additionneurs capables d'additionner sur le nombre de bits que l'on souhaite. Par exemple, dans le schéma ci-dessous permet de créer un additionneur sur 4 bits, en combinant 4 additionneurs 1 bit. Vous noterez qu'ici, plutôt que de reproduire quatre fois tout le circuit additionneur vu précédemment, on utilise le composant prédéfini du simulateur (où l'on retrouve les 3 entrées et les deux sorties).

Plus précisémenent, le circuit ci-dessous permet de faire l'addition de deux nombres $x = (x_4 \, x_3 \, x_2 \, x_1)_2$ et $y = (y_4 \, y_3 \, y_2 \, y_1)_2$ de 4 bits, le résultat de la somme étant égal à $s = (s_5 \, s_4 \, s_3 \, s_2 \, s_1)_2$. 

*Vous pouvez modifier les valeurs des bits d'entrée (en haut) pour vérifier que les sorties (en bas) correspondent bien à la somme. Par exemple, vérifiez que $(0110)_2 + (1011)_2$ est bien égal à $(10001)_2$ en observant bien la retenue sortante qui devient la retenue entrante de l'additionneur suivant.*

<div class="logic-editor centre" style="height: 300px;">
  <logic-editor mode="connect">
    <script type="application/json">
      { // JSON5
        v: 6,
        components: {
          adder0: {type: 'adder', pos: [550, 140], in: '6-8', out: [9, 10]},
          adder1: {type: 'adder', pos: [425, 140], in: '11-13', out: [14, 15]},
          adder2: {type: 'adder', pos: [305, 140], in: '16-18', out: [19, 20]},
          adder3: {type: 'adder', pos: [185, 140], in: '21-23', out: [24, 25]},
          in0: {type: 'in', pos: [165, 55], orient: 's', id: 1, name: 'x4'},
          in1: {type: 'in', pos: [205, 55], orient: 's', id: 0, name: 'y4'},
          in2: {type: 'in', pos: [285, 55], orient: 's', id: 2, name: 'x3'},
          in3: {type: 'in', pos: [325, 55], orient: 's', id: 3, name: 'y3'},
          in4: {type: 'in', pos: [405, 55], orient: 's', id: 4, name: 'x2'},
          in5: {type: 'in', pos: [445, 55], orient: 's', id: 5, name: 'y2'},
          in6: {type: 'in', pos: [530, 55], orient: 's', id: 26, name: 'x1'},
          in7: {type: 'in', pos: [570, 55], orient: 's', id: 27, name: 'y1'},
          in8: {type: 'in', pos: [655, 140], orient: 'w', id: 28, name: 'Cin', isConstant: true},
          out0: {type: 'out', pos: [550, 225], orient: 's', id: 29, name: 's1'},
          out1: {type: 'out', pos: [425, 225], orient: 's', id: 30, name: 's2'},
          out2: {type: 'out', pos: [305, 225], orient: 's', id: 31, name: 's3'},
          out3: {type: 'out', pos: [185, 225], orient: 's', id: 32, name: 's4'},
          out4: {type: 'out', pos: [75, 140], orient: 'w', id: 33, name: 's5'},
        },
        wires: [[10, 13], [15, 18], [20, 23], [1, 21], [0, 22], [2, 16], [3, 17], [4, 11], [5, 12], [26, 6], [27, 7], [28, 8], [9, 29], [14, 30], [19, 31], [24, 32], [25, 33]]
      }
    </script>
  </logic-editor>
</div>

<p class="legende">Réalisé avec le simulateur <em>logic.modulo-info.ch</em>.
<br><a href="https://logic.modulo-info.ch/?mode=design&data=N4NwXAbANAxg9gWwA5wHYFNUBcDOZgCGAJkegE4AM+WAnkumAOTGlmNQp4DaArDxVACMAFgoBdKAEtUTCAFoAHOzgBXLGC4BOIeIC+UFuUHU6DZiXLtOG4QCYeQ0ROlNBguYIDMytRpFCeMX1DMlsTeiYQqzhuTwoHEXEpGUZBeUElKFV1LkFtWz0DCzJPcLMojhi-BQSnZKZbd1tvLN8uW2EoeyDkqmBaCMZpaO40hz4JODJJTHVGHHZJIjBBKFQCBDMAD2FGfWljftMmYcruAvHArOnZpgWpZYF1zaYaXf3UMKPB0+t2mqgE2uM2wd0Wy1saw2228H1K3zMvyqXE89kBVymILm9yWYE8UJejBosOSwjKJ1QIxs8XRkxuoPm4LAnWe21se2SPHJQ0pZxswkudKxYIeYAcrNe7I+EG5SO4PDitOBt0Zots0AljC2gg50gA7LLeX8eHqBEDMSqcRC9QSzDQdR8FIaqVwIHxHEkLQyAO5M2wKW1MADCp0kOCDaBwWAIoKwZBU6H02T6AzM2RdfAEtm6yoZVrAtm0mpwDtaWEOqaY6b5XDsDmzGPp2KZiuLUrLX0rjGrfzi9ZzXubos8q2LJOy8K7PeRGX7jeFqtxqMD83eZbJCKrahdetqnqbTF9w-xxZ4e303skZHQ3FyAi8ElyCQUj4KXU8j9WjUfWdsr6EECPvigh6o+nRuI+CR-lA7TQIBMG2DaoEIQGL4wfkmifp0cSftoI6vth0HtA4ngfkEQA" target="_blank">Ouvrir ce circuit avec le simulateur</a></p>

# Les circuits *mémoires* (hors programme)

Ce n'est pas du tout au programme, mais sachez qu'avec les portes logiques, on peut aussi créer des circuits qui *stockent* des informations. Par exemple, un *verrou* est un circuit permettant mémoriser des informations. Autrement dit, c'est un circuit qui permet, dans certaines conditions, de maintenir la valeur de sa sortie malgré des changements de valeurs d'entrée. 

En assemblant des verrous, on peut réaliser des *compteurs*, des *registres* ou encore des *mémoires*. Par exemple, le verrou ci-dessous permet de mémoriser 1 bit en mémoire, et il s'agit juste d'une combinaison de portes logiques :

<img class="centre image-responsive" src="data/verrou-D.svg" alt="circuit de type bascule">
<p class="legende"><strong>Un circuit <em>verrou-D</em></strong></p>

>Sur ce schéma, la porte
><img class="centre image-responsive" alt="porte NOR" src="data/porte_nor.svg">
>correspond à une porte logique NON OU (ou NOR en anglais, pour *Not OR*), qui n'est autre que la succession d'une porte OU puis d'une porte NON, donc équivalent à :
><img class="centre image-responsive" alt="porte NOR" src="data/porte_ou_non.svg">

En pratique, lorsque l'entrée E est mise dans l'état 1, la valeur de l'entrée D (comme *data*) est mémorisée par le circuit et cette valeur peut être lue sur la sortie Q. Si on remet E à 0 et que l'on modifie la valeur de D, alors la sortie Q n'est pas modifiée, la valeur est donc bien mémorisée, jusqu'à ce que l'on provoque la mémorisation d'une nouvelle valeur en mettant E à 1.

*Le circuit a été reproduit ci-dessous (sans la sortie $\overline{Q}$, qui n'a pas d'importance dans notre cas), cela vous permet de tester et comprendre ce principe de mémorisation.*

<div class="logic-editor centre" style="height: 190px;">
  <logic-editor mode="tryout" tabindex="-1">
    <script type="application/json">
      { // JSON5
        v: 6,
        opts: {wireStyle: 'straight'},
        components: {
          nor0: {type: 'or', pos: [315, 55], in: [10, 11], out: 12},
          nor1: {type: 'or', pos: [315, 145], in: [13, 14], out: 15},
          and0: {type: 'and', pos: [235, 45], in: [16, 17], out: 18},
          out0: {type: 'out', pos: [505, 55], id: 19, name: 'Q'},
          and1: {type: 'and', pos: [235, 155], in: [0, 1], out: 2},
          not0: {type: 'not', pos: [155, 35], in: 3, out: 4},
          in0: {type: 'in', pos: [55, 35], id: 5, name: 'D', val: 1},
          in1: {type: 'in', pos: [55, 95], id: 6, name: 'E', val: 1},
          not1: {type: 'not', pos: [395, 55], in: 7, out: 8},
          not2: {type: 'not', pos: [395, 145], in: 9, out: 20},
        },
        wires: [[18, 10], [2, 14], [4, 16], [5, 3], [5, 1, {via: [[115, 35], [115, 165]]}], [6, 17, {via: [[195, 95]]}], [6, 0, {via: [[195, 95]]}], [12, 7], [15, 9], [8, 13, {via: [[435, 75, 'w'], [275, 115, 'w']]}], [20, 11, {via: [[435, 125, 'w'], [275, 85, 'w']]}], [8, 19]]
      }
    </script>
  </logic-editor>
</div>
<p class="legende">
  <strong>Un circuit <em>verrou-D</em></strong>
  <br>Réalisé avec le simulateur <em>logic.modulo-info.ch</em>
  <br><a href="https://logic.modulo-info.ch/?mode=full&data=N4NwXAbANA9gDgFwM5mAdwJYCcCmBlBATwBscwByJBLAQwwHMALBcgXygGMYBbOGAOxz9kqfjCwAGVEThly48lD4oA2gGYAjAFYoWrQF0oGfmBUaJUDRsMwArgjAaATOzFYN0wrIoKlMVZo6GgAsBkYmZmqWwTb2jlrsNPwAJlLAMnJJyYrKpk5qOqGGxqYa0BoA7LEOGgAc7HYIaRk+9jn+ploSOnrFyY4AnFD8NNxyAIpsUFke6V6ZKe2q+UG94aYW1rBxLsMwTZ7e5GIsfqraOgXFJlGNYMHsxs3zFMZLnZdhGP06I2MUABFFCAaMRHI9+LMWuQ3mcPlABl9+tA-nIAKLA0HgvYIKEvY77d7qRG6L4mCrbBz1HFOQ5yE5EtQkkJksBDO5OCSsdiYXCqMy1SwSQwqJzREXBSwQEWXGWWKCgDA0UxmbRQK5QVVBCAGfSsEXlCmK5UqMwkxH6PUGqAWY0qjTm3X6zXOKBVF06AYiwUaKJ203BApunTkNDkEVOCpBNWh8NWzWcywaBUgJUqwNBJwhsMRqNQWrZuPOlQ+r16oA" target="_blank">Ouvrir ce circuit avec le simulateur</a>
</p>

>Pour en savoir plus sur ces notions, vous pouvez lire le chapitre 15 (page 182) du livre suivant : [Informatique et Sciences du Numérique, Gilles Dowek](https://wiki.inria.fr/wikis/sciencinfolycee/images/a/a7/Informatique_et_Sciences_du_Num%C3%A9rique_-_Sp%C3%A9cialit%C3%A9_ISN_en_Terminale_S._version_Python.pdf).

# Bilan

- Les **booléens** sont des variables qui ne possèdent que deux valeurs : vrai et faux.
- La théorie mathématique derrière l'utilisation et les calculs sur les booléens a été élaborée par George Boole (d'où le nom de ce type de variables).
- Des **opérateurs booléens** permettent de faire des opérations logiques sur les booléens : les trois opérateurs de base sont ***non*** (la négation), ***ou*** (la disjonction), ***et*** (la conjonction).
- Un opérateur booléen peut se décrire par sa **table de vérité** qui liste tous les résultats possibles en fonction des différentes combinaisons d'entrées.
- Un ordinateur est composé de **transistors**, qui sont des composants électroniques agissant comme des interrupteurs qui laissent ou non passer le courant. Ces deux états possibles se notent respectivement 1 et 0. Ainsi, un ordinateur peut manipuler des bits en s'appuyant sur la théorie développée par George Boole.
- En reliant des transistors de la bonne manière on peut créer des circuits appelés **portes logiques**. Ces portes permettent de réaliser les opérateurs booléens, et donc de réaliser des opérations logiques entre les bits.
- À partir de ces portes logiques, on peut créer des circuits plus complexes permettant d'effectuer des additions, des multiplications, ... L'*unité arithmétique et logique* d'un processeur contient ces différents circuits, absolument nécessaires au fonctionnement de l'ordinateur.
- (On peut même mémoriser des informations avec des portes logiques, et donc réaliser des composants mémoires (RAM, registres,etc.), ce qui montre bien que les portes logiques constituent les briques de base d'un ordinateur.)

La vidéo ci-dessous est un excellent résumé sur les **portes logiques** :

<div class="video-responsive">
  <div class="youtube_player centre" videoID="iTH39L2d7bg" width="560" height="315" theme="light" rel="1" controls="1" showinfo="1" autoplay="0"></div>
</div>
<p class="impression">
  Lien vers la vidéo : <a href="https://youtu.be/iTH39L2d7bg">https://youtu.be/iTH39L2d7bg</a>
</p> 

---
**Références** :

- Livre *Informatique et Sciences du Numérique, Terminale S*, de Gilles Dowek, éditions Eyrolles, disponible en [version PDF](https://wiki.inria.fr/wikis/sciencinfolycee/images/a/a7/Informatique_et_Sciences_du_Num%C3%A9rique_-_Sp%C3%A9cialit%C3%A9_ISN_en_Terminale_S._version_Python.pdf).
- Cours et exercices de Romain Janvier sur les [Circuits logiques](https://nsi.janviercommelemois.fr/fichiers_pdf/feuille-circuits-logiques.pdf) et sur [Python - Algèbre de Boole](https://nsi.janviercommelemois.fr/fichiers_pdf/feuille-python-algebre-boole.pdf).
- Le simulateur logique en ligne [https://logic.modulo-info.ch/](https://logic.modulo-info.ch/) développé par Jean-Philippe Pellet.
- Article Wikipédia sur l'[Additionneur](https://fr.wikipedia.org/wiki/Additionneur).
- Livre *Numérique et Sciences Informatiques, Première NSI* de T. Balabonski, S. Conchon, J.-C. Filliâtre et K. Nguyen, éditions Ellipses. Site du livre : [www.nsi-premiere.fr](www.nsi-premiere.fr).

**Crédits** : Les schémas des circuits et portes logiques dont la licence n'est pas précisée, sont soit sous licence CC0, soit dans le domaine public, soit une modification personnelle d'une telle image. En particulier, les schémas américains des portes logiques sont disponibles dans l'article Wikipédia sur les [Portes logiques](https://fr.wikipedia.org/wiki/Porte_logique), les schémas européens sont accessibles en suivant ce lien : [https://commons.wikimedia.org/wiki/Category:IEC_Logic_Gates](https://commons.wikimedia.org/wiki/Category:IEC_Logic_Gates).

---
Germain Becker, Lycée Emmanuel Mounier, Angers.

![Licence Creative Commons](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)