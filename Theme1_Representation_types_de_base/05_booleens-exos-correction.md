Booléens : opérateurs et portes logiques - EXERCICES
====================================================

# Opérateurs booléens

## ✏️ Exercice 1 : Distributivité de *et* sur *ou*

Les opérateurs booléens vérifient la propriété de **distributivité** suivante, qui est vraie pour tous booléens *a*, *b* et *c* : 

$$\text{a et (b ou c) = (a et b) ou (a et c)}.$$

Recopier et compléter les tables de vérité suivantes pour démontrer cette égalité :

<div style="display:flex; flex-wrap:wrap; gap:2em; justify-content:center;">
    <div>
        <table>
            <thead>
                <tr>
                    <th>a</th>
                    <th>b</th>
                    <th>c</th>
                    <th>b ou c</th>
                    <th>a et (b ou c)</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>0</td>
                    <td>0</td>
                    <td>1</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>0</td>
                    <td>1</td>
                    <td>0</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>0</td>
                    <td>1</td>
                    <td>1</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>1</td>
                    <td>0</td>
                    <td>0</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>1</td>
                    <td>0</td>
                    <td>1</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>1</td>
                    <td>1</td>
                    <td>0</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>1</td>
                    <td>1</td>
                    <td>1</td>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
<!-- | a | b | c | b ou c | a et (b ou c) |
|---|---|---|--------|---------------|
| 0 | 0 | 0 |        |               |
| 0 | 0 | 1 |        |               |
| 0 | 1 | 0 |        |               |
| 0 | 1 | 1 |        |               |
| 1 | 0 | 0 |        |               |
| 1 | 0 | 1 |        |               |
| 1 | 1 | 0 |        |               |
| 1 | 1 | 1 |        |               | -->
    </div>
    <div>
        <table>
            <thead>
                <tr>
                    <th>a</th>
                    <th>b</th>
                    <th>c</th>
                    <th>a et b</th>
                    <th>a et c</th>
                    <th>(a et b) ou (a et c)</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>0</td>
                    <td>0</td>
                    <td>1</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>0</td>
                    <td>1</td>
                    <td>0</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>0</td>
                    <td>1</td>
                    <td>1</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>1</td>
                    <td>0</td>
                    <td>0</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>1</td>
                    <td>0</td>
                    <td>1</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>1</td>
                    <td>1</td>
                    <td>0</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>1</td>
                    <td>1</td>
                    <td>1</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
<!-- | a | b | c | a et b | a et c | (a et b) ou (a et c) |
|---|---|---|--------|--------|----------------------|
| 0 | 0 | 0 |        |        |                      |
| 0 | 0 | 1 |        |        |                      |
| 0 | 1 | 0 |        |        |                      |
| 0 | 1 | 1 |        |        |                      |
| 1 | 0 | 0 |        |        |                      |
| 1 | 0 | 1 |        |        |                      |
| 1 | 1 | 0 |        |        |                      |
| 1 | 1 | 1 |        |        |                      | -->
    </div>
</div>

<details markdown="1" class="effacer-impression en-savoir-plus">
    <summary>Voir la correction</summary>

Table de $\text{a et (b ou c)}$ :

| a | b | c | b ou c | a et (b ou c) |
|---|---|---|--------|---------------|
| 0 | 0 | 0 | 0      | 0             |
| 0 | 0 | 1 | 1      | 0             |
| 0 | 1 | 0 | 1      | 0             |
| 0 | 1 | 1 | 1      | 0             |
| 1 | 0 | 0 | 0      | 0             |
| 1 | 0 | 1 | 1      | 1             |
| 1 | 1 | 0 | 1      | 1             |
| 1 | 1 | 1 | 1      | 1             |

Table de $\text{(a et b) ou (a et c)}$ :

| a | b | c | a et b | a et c | (a et b) ou (a et c) |
|---|---|---|--------|--------|----------------------|
| 0 | 0 | 0 | 0      | 0      | 0                    |
| 0 | 0 | 1 | 0      | 0      | 0                    |
| 0 | 1 | 0 | 0      | 0      | 0                    |
| 0 | 1 | 1 | 0      | 0      | 0                    |
| 1 | 0 | 0 | 0      | 0      | 0                    |
| 1 | 0 | 1 | 0      | 1      | 1                    |
| 1 | 1 | 0 | 1      | 0      | 1                    |
| 1 | 1 | 1 | 1      | 1      | 1                    |

Les deux dernières colonnes sont identiques donc les tables de vérité de $\text{a et (b ou c)}$ et $\text{(a et b) ou (a et c)}$ sont identiques, ce qui prouve l'égalité entre ces deux expressions booléennes et donc que l'opérateur $\text{et}$ se distribue sur l'opérateur $\text{ou}$. 

</details>

## ✏️ Exercice 2 : Opérateur *xor* avec *et*, *ou* et *non*

**1.** Rappeler la table de vérité de l'opérateur ***xor***.

**2.** Construire, comme dans l'exercice précédent, la table de vérité de ***(a et non b) ou (non a et b)*** et vérifier que l'égalité suivante est vraie :

$$\text{a xor b = (a et non b) ou (non a et b)}.$$

<details markdown="1" class="effacer-impression en-savoir-plus">
    <summary>Voir la correction</summary>

**1.** Table de vérité de ***xor*** :

| *a* | *b* | *a xor b*  |
|---|---|----------|
| 0 | 0 | 0        |
| 1 | 0 | 1        |
| 0 | 1 | 1        |
| 1 | 1 | 0        |

**2.** Table de vérité de ***(a et non b) ou (non a et b)*** :

| a | b | non a | non b | a et non b | non a et b | (a et non b) ou (non a et b) |
|---|---|-------|-------|------------|------------|------------------------------|
| 0 | 0 | 1     | 1     | 0          | 0          | 0                            |
| 1 | 0 | 0     | 1     | 1          | 0          | 1                            |
| 0 | 1 | 1     | 0     | 0          | 1          | 1                            |
| 1 | 1 | 0     | 0     | 0          | 0          | 0                            |

Les deux dernières colonnes sont identiques donc on a bien $\text{a xor b = (a et non b) ou (non a et b)}$, ce qui prouve que l'opérateur ***xor*** peut s'exprimer à partir des opérateurs ***non***, ***et*** et ***ou***.

</details>

## ✏️ Exercice 3 : Évaluation paresseuse

*Dans cet exercice, les expressions sont écrites dans le langage Python.*

**1.** Expliquer pourquoi l'expression `3 == 3 or x == y` est vraie pour toute valeur de `x` et de `y`.

**2.** Expliquer pourquoi l'expression `1 == 2 and x == y` est fausse pour toute valeur de `x` et de `y`.

<blockquote class="information" markdown="1">

Lorsque Python évalue une expression booléenne, il le fait de façon **paresseuse**. C'est-à-dire que si la partie gauche d'un `or` est vraie, il n'évalue pas la partie droite. De même, si la partie gauche d'un `and` est fausse, la partie droite n'est pas évaluée.

On peut le voir facilement : dans l'expression booléenne suivante, si la partie de droite était évaluée, Python aurait renvoyé une erreur de type `ZeroDivisionError` :

<pre class="language-python"><code>>>> x = 0
>>> 2 == 2 or 1/x > 0  # 2 == 2 est vrai donc 1/x > 0 n'est pas évalué
True</code></pre>

</blockquote>

<details markdown="1" class="effacer-impression en-savoir-plus">
    <summary>Voir la correction</summary>

**1.** La partie de gauche, `3 == 3`, est toujours vraie.  
Donc l'expression est équivalente à `True or x == y`.  
Or `True or NimporteQuoi` est toujours égal à `True` (puisque `1 or 0 = 1` et `1 or 1 = 1`).  
L'évaluation de Python étant *paresseuse*, l'interpréteur n'évalue même pas la partie de droite `x == y`, car la partie de gauche suffit à donner la réponse.

**2.** La partie de gauche, `1 == 2`, est toujours fausse.  
Donc l'expression est équivalente à `False and x == y`.  
Or `False and NimporteQuoi` est toujours égal à `False` (puisque `0 and 0 = 0` et `0 and 1 = 0`).  
L'évaluation de Python étant *paresseuse*, l'interpréteur n'évalue même pas la partie de droite `x == y`, car la partie de gauche suffit à donner la réponse.

</details>

## ✏️ Exercice 4 : Avec Python

**1.** Écrire en Python une fonction `xor(a, b)` qui renvoie la valeur de l'opération logique ***a xor b***, où *a* et *b* sont deux booléens représentés par `0` et `1`.

*Exemples :*

```python
>>> xor(1, 0)
1
>>> xor(1, 1)
0
```

**2.** L'opérateur ***non-et***, que l'on écrit ***nand*** en anglais (contraction de _**n**ot **and**_), est la succession des opérateurs ***et*** puis ***non***. On a donc pour tout booléen *a* et *b* : 

$$\text{a nand b = non (a et b)}$$

**2a.** Donner la table de vérité de l'opérateur ***nand***.

**2b.** Écrire une fonction Python `nand(a, b)` qui renvoie la valeur de ***a nand b***, où *a* et *b* sont deux booléens représentés par `0` et `1`.

*Exemple* :

```python
>>> nand(1, 0)
1
```

<details markdown="1" class="effacer-impression en-savoir-plus">
    <summary>Voir la correction</summary>

**1.** Voici une fonction `xor` qui convient :

```python
def xor(a, b):
	if a != b:
		return 1
	else:
		return 0
```

**2a.** Construire la table de vérité de ***nand*** revient à construire celle de $\text{non (a et b)}$ :

| a | b | a et b | non(a et b) |
|---|---|--------|-------------|
| 0 | 0 | 0      | 1           |
| 1 | 0 | 0      | 1           |
| 0 | 1 | 0      | 1           |
| 1 | 1 | 1      | 0           |

**2b.** Voici une fonction `nand` qui convient :

```python
def nand(a, b):
	if a == b:
		return 0
	else:
		return 1
```

Et une autre qui utilise les opérateurs `and` et `not` de Python :

```python
def nand(a, b):
	return not(a and b)
```

</details>

# Portes et circuits logiques

## ✏️ Exercice 5 : Porte NAND

Une porte NAND permet de réaliser l'opération logique ***nand*** (ou ***non-et*** en français). On rappelle (voir exercice précédent) que cet opérateur est défini de la manière suivante :

$$\text{a nand b = non (a et b)}$$

**1.** Construire avec le simulateur une porte NAND (ou NON-ET) en utilisant une porte NON et une porte ET.  *Le circuit est à recopier sur votre feuille.*

<div class="logic-editor centre" style="height: 420px;">
  <logic-editor mode="design" showonly="and not in out">
    <script type="application/json">
      { // JSON5
        v: 6
      }
    </script>
  </logic-editor>
</div>

**2**. Vérifier, avec le simulateur, que le circuit est correct en comparant avec la table de vérité construite dans l'exercice précédent.

<details markdown="1" class="effacer-impression en-savoir-plus">
    <summary>Voir la correction</summary>

**1.** Voici un circuit NAND avec portes NON et ET :

<img class="centre image-responsive" alt="circuit nand" src="data/nand_ex5_correction.svg">

**2.** En testant toutes les combinaisons possibles on retrouve bien la table de vérité de l'opérateur ***nand*** (cf. exercice précédent).

<div class="logic-editor centre">
  <logic-editor mode="tryout">
    <script type="application/json">
      { // JSON5
        v: 6,
        opts: {showGateTypes: true},
        components: {
          and0: {type: 'and', pos: [185, 55], in: [0, 1], out: 2},
          in0: {type: 'in', pos: [55, 35], id: 3, name: 'a'},
          in1: {type: 'in', pos: [55, 75], id: 4, name: 'b'},
          out0: {type: 'out', pos: [335, 55], id: 5},
          not0: {type: 'not', pos: [265, 55], in: 6, out: 7},
        },
        wires: [[3, 0], [4, 1], [2, 6], [7, 5]]
      }
    </script>
  </logic-editor>
</div>

</details>

## ✏️ Exercice 6 : Loi de De Morgan

L'objectif de l'exercice est de démontrer la loi de [De Morgan](https://fr.wikipedia.org/wiki/Auguste_De_Morgan) suivante : 

>Pour tous booléens $a$ et $b$, on a :  $\text{non (a et b) = (non a) ou (non b)}$.


Le circuit ci-dessous correspond à l'expression booléenne ***non (a et b)*** (celle de gauche dans l'égalité) :

<div class="logic-editor centre" style="height: 140px;">
  <logic-editor mode="tryout">
    <script type="application/json">
      { // JSON5
        v: 6,
        opts: {showGateTypes: true},
        components: {
          in0: {type: 'in', pos: [70, 45], id: 0, name: 'a'},
          in1: {type: 'in', pos: [70, 95], id: 1, name: 'b'},
          and0: {type: 'and', pos: [190, 70], in: [2, 3], out: 4},
          not0: {type: 'not', pos: [270, 70], in: 6, out: 7},
          out0: {type: 'out', pos: [340, 70], id: 8},
        },
        wires: [[0, 2], [1, 3], [4, 6], [7, 8]]
      }
    </script>
  </logic-editor>
</div>

**1.** Construire le circuit correspondant à l'expression booléenne ***(non a) ou (non b)*** (celle de droite dans l'égalité). *Le circuit est à recopier sur votre feuille.*

<div class="logic-editor centre" style="height: 420px;">
  <logic-editor mode="design" showonly="or not in out">
    <script type="application/json">
      { // JSON5
        v: 6
      }
    </script>
  </logic-editor>
</div>

**2.** Vérifier, en utilisant le simulateur, que les deux circuits sont équivalents, c'est-à-dire qu'ils ont la même table de vérité.

<details markdown="1" class="effacer-impression en-savoir-plus">
    <summary>Voir la correction</summary>

**1.** Voici un circuit correspondant à l'expression ***(non a) ou (non b)*** :

<div class="logic-editor centre" style="height: 135px">
  <logic-editor mode="tryout">
    <script type="application/json">
      { // JSON5
        v: 6,
        opts: {showGateTypes: true},
        components: {
          in0: {type: 'in', pos: [55, 40], id: 3, name: 'a'},
          in1: {type: 'in', pos: [55, 95], id: 4, name: 'b'},
          out0: {type: 'out', pos: [365, 70], id: 5},
          not0: {type: 'not', pos: [160, 40], in: 6, out: 7},
          not1: {type: 'not', pos: [160, 95], in: 8, out: 9},
          or0: {type: 'or', pos: [295, 70], in: [10, 11], out: 12},
        },
        wires: [[4, 8], [3, 6], [12, 5], [7, 10], [9, 11]]
      }
    </script>
  </logic-editor>
</div>

**2.** En testant toutes les combinaisons possibles on trouve la table de vérité suivante :

| a | b | (non a) ou (non b) |
|---|---|----------|
| 0 | 0 | 1        |
| 1 | 0 | 1        |
| 0 | 1 | 1        |
| 1 | 1 | 0        |

Cela correspond bien à la table de vérité de ***non (a et b)*** (équivalent à l'opérateur ***non-et***, ou ***nand*** en anglais).  
Donc on a bien : $\text{non (a et b) = (non a) ou (non b)}$

</details>



## ✏️ Exercice 7 : Porte XOR avec ET, OU  et NON

**1.** Construire le circuit correspondant à l'expression ***(a et non b) ou (non a et b)***. *Le circuit est à recopier sur votre feuille.*

<div class="logic-editor centre" style="height: 500px;">
  <logic-editor mode="design" showonly="or not in out and">
    <script type="application/json">
      { // JSON5
        v: 6
      }
    </script>
  </logic-editor>
</div>

**2.** Vérifier, en utilisant le simulateur, que ce circuit est équivalent à une porte XOR.


<details markdown="1" class="effacer-impression en-savoir-plus">
    <summary>Voir la correction</summary>

**1.** Voici un circuit correspondant à l'expression ***(a et non b) ou (non a et b)*** :

<div class="logic-editor centre" style="height: 215px">
  <logic-editor mode="tryout">
    <script type="application/json">
      { // JSON5
        v: 6,
        opts: {showGateTypes: true},
        components: {
          in0: {type: 'in', pos: [55, 40], id: 3, name: 'a'},
          in1: {type: 'in', pos: [55, 175], id: 4, name: 'b'},
          out0: {type: 'out', pos: [480, 105], id: 5},
          not0: {type: 'not', pos: [155, 40], in: 6, out: 7},
          not1: {type: 'not', pos: [160, 175], in: 8, out: 9},
          or0: {type: 'or', pos: [410, 105], in: [10, 11], out: 12},
          and0: {type: 'and', pos: [285, 80], in: [13, 14], out: 15},
          and1: {type: 'and', pos: [285, 135], in: [16, 17], out: 18},
        },
        wires: [[4, 8], [3, 6], [12, 5], [9, 14, {via: [[200, 90, 'n']]}], [7, 16, {via: [[220, 40, 's'], [220, 125, 's']]}], [4, 17, {via: [[85, 145]]}], [3, 13, {via: [[85, 70]]}], [15, 10], [18, 11]]
      }
    </script>
  </logic-editor>
</div>

**2.** En testant toutes les combinaisons possibles on retrouve bien la table de vérité de l'opérateur ***xor*** à savoir :

| *a* | *b* | *a xor b*  |
|---|---|----------|
| 0 | 0 | 0        |
| 1 | 0 | 1        |
| 0 | 1 | 1        |
| 1 | 1 | 0        |

Cela prouve que l'on peut construire une porte XOR à partir des portes ET, NON et OU.

</details>


## ✏️ Exercice 8 : Comparateur

Dans cet exercice on considère le circuit suivant, appelé **comparateur**, qui permet de comparer des mots binaires (a1, a0) et (b1, b0) de longueur 2. Le bit de sortie :
- vaut 1 si  (a1, a0) = (b1, b0) ;
- vaut 0 sinon.

<img class="centre image-responsive" alt="circuit comparateur" src="data/comparateur.svg" width="600">

**1.** Construire ce circuit avec le simulateur.

<div class="logic-editor centre" style="height: 550px;">
  <logic-editor mode="design" showonly="xor not and xnor in out">
    <script type="application/json">
      { // JSON5
        v: 6
      }
    </script>
  </logic-editor>
</div>

**2.** Recopier et compléter (en utilisant directement le simulateur) la table de vérité de ce circuit.

| a1 | a0 | b1 | b0 | sortie |
|----|----|----|----|--------|
| 0  | 0  | 0  | 0  |        |
| 0  | 0  | 0  | 1  |        |
| 0  | 0  | 1  | 0  |        |
| 0  | 0  | 1  | 1  |        |
| 0  | 1  | 0  | 0  |        |
| 0  | 1  | 0  | 1  |        |
| 0  | 1  | 1  | 0  |        |
| 0  | 1  | 1  | 1  |        |
| 1  | 0  | 0  | 0  |        |
| 1  | 0  | 0  | 1  |        |
| 1  | 0  | 1  | 0  |        |
| 1  | 0  | 1  | 1  |        |
| 1  | 1  | 0  | 0  |        |
| 1  | 1  | 0  | 1  |        |
| 1  | 1  | 1  | 0  |        |
| 1  | 1  | 1  | 1  |        |

**3.** Expliquer, en s'appuyant sur la table de vérité, pourquoi ce circuit permet bien de comparer deux mots binaires de longueur 2.

>On peut comparer des mots de longueur supérieure à deux en ajoutant d'autres portes NON XOR (notée aussi *NON OU exclusif* ou encore *XNOR*).

**4.** Donner une expression booléenne écrite en Python qui nécessiterait d'utiliser un tel circuit logique pour être évaluée.

<details markdown="1" class="effacer-impression en-savoir-plus">
    <summary>Voir la correction</summary>

**1.** Voici le circuit comparateur (2 bits) proposé :

<div class="logic-editor centre" style="height: 210px">
  <logic-editor mode="tryout">
    <script type="application/json">
      { // JSON5
        v: 6,
        opts: {showGateTypes: true},
        components: {
          in0: {type: 'in', pos: [70, 35], id: 0, name: 'a0'},
          in1: {type: 'in', pos: [70, 75], id: 1, name: 'a1'},
          in2: {type: 'in', pos: [70, 135], id: 2, name: 'b0'},
          in3: {type: 'in', pos: [70, 175], id: 3, name: 'b1'},
          xor0: {type: 'xor', pos: [235, 45], in: [4, 5], out: 6},
          xor1: {type: 'xor', pos: [235, 165], in: [7, 8], out: 9},
          not0: {type: 'not', pos: [315, 45], in: 10, out: 11},
          not1: {type: 'not', pos: [315, 165], in: 12, out: 13},
          and0: {type: 'and', pos: [435, 105], in: [14, 15], out: 16},
          out0: {type: 'out', pos: [505, 105], id: 17, name: 'sortie'},
        },
        wires: [[6, 10], [9, 12], [0, 4], [1, 7], [2, 5], [3, 8], [11, 14], [13, 15], [16, 17]]
      }
    </script>
  </logic-editor>
</div>

**2.** En utilisant le simulateur, on trouve :

| a1 | a0 | b1 | b0 | sortie |
|----|----|----|----|--------|
| 0  | 0  | 0  | 0  | 1      |
| 0  | 0  | 0  | 1  | 0      |
| 0  | 0  | 1  | 0  | 0      |
| 0  | 0  | 1  | 1  | 0      |
| 0  | 1  | 0  | 0  | 0      |
| 0  | 1  | 0  | 1  | 1      |
| 0  | 1  | 1  | 0  | 0      |
| 0  | 1  | 1  | 1  | 0      |
| 1  | 0  | 0  | 0  | 0      |
| 1  | 0  | 0  | 1  | 0      |
| 1  | 0  | 1  | 0  | 1      |
| **1**  | **0**  | **1**  | **1**  | **0**      |
| 1  | 1  | 0  | 0  | 0      |
| 1  | 1  | 0  | 1  | 0      |
| 1  | 1  | 1  | 0  | 0      |
| 1  | 1  | 1  | 1  | 1      |

**3.** On voit que la sortie vaut 1 si seulement si le couple (a1, a0) est égal au couple (b1, b0). Le circuit est donc bien un comparateur dont la sortie est Vraie si les mots binaires (a1, a0) et (b1, b0) sont égaux.

**4.** Par exemple, `2 == 3` est une expression booléenne Python qui peut s'évaluer avec un tel circuit car 2 et 3 s'écrivent respectivement 10 et 11 en binaire, soit sur 2 bits. On aurait donc par exemple : a0 = 1, a1 = 0, b0 = 1 et b1 = 1 et le résultat est `False` (sortie = 0) puisque (1, 0) est différent de (1, 1). Cela correspond à la ligne en gras dans la table de vérité ci-dessus.  
En revanche `2 == 2` serait évalué à `True` car (1, 0) = (1, 0).

</details>


## ✏️ Exercice 9 : Exploration (optionnel)

En utilisant des portes NON, ET, OU, dessiner un circuit logique qui a 4 entrées A, B, C et D et dont la sortie S vaut 1 si et seulement si au moins une des entrées est non nulle.

<div class="logic-editor centre" style="height: 350px">
  <logic-editor mode="design" showonly="not and or">
    <script type="application/json">
      { // JSON5
        v: 6,
        components: {
          in0: {type: 'in', pos: [80, 75], id: 0, name: 'A'},
          in1: {type: 'in', pos: [80, 120], id: 1, name: 'B'},
          in2: {type: 'in', pos: [80, 165], id: 2, name: 'C'},
          in3: {type: 'in', pos: [80, 210], id: 3, name: 'D'},
          out0: {type: 'out', pos: [630, 145], id: 4, name: 'S'},
        }
      }
    </script>
  </logic-editor>
</div>


<details markdown="1" class="effacer-impression en-savoir-plus">
    <summary>Voir la correction</summary>

S vaut 1 si et seulement si au moins une des entrées A, B, C, D est non nulle est équivalent à : S vaut 0 dans le seul cas suivant : A = B = C = D = 0. 

Cela correspond à l'expression booléenne ***A ou B ou C ou D*** que l'on peut construire de la façon suivante :

<div class="logic-editor centre" style="height: 225px">
  <logic-editor mode="tryout">
    <script type="application/json">
      { // JSON5
        v: 6,
        components: {
          in0: {type: 'in', pos: [60, 45], id: 0, name: 'A', val: 1},
          in1: {type: 'in', pos: [60, 90], id: 1, name: 'B'},
          in2: {type: 'in', pos: [60, 135], id: 2, name: 'C'},
          in3: {type: 'in', pos: [60, 180], id: 3, name: 'D'},
          out0: {type: 'out', pos: [335, 110], id: 4, name: 'sortie'},
          and1: {type: 'or', pos: [160, 65], in: [8, 9], out: 10},
          and0: {type: 'or', pos: [160, 155], in: [11, 12], out: 13},
          or0: {type: 'or', pos: [265, 110], in: [14, 15], out: 16},
        },
        wires: [[0, 8], [1, 9], [2, 11], [3, 12], [10, 14], [13, 15], [16, 4]]
      }
    </script>
  </logic-editor>
</div>

</details>



# Exercices d'approfondissement (optionnel)

Pour celles et ceux qui souhaiteraient aller approfondir les notions de ce chapitre, vous pouvez chercher les exercices suivants, élaborés par Romain Janvier, qui propose :

- de construire pas à pas une UAL (unité arithmétique et logique) complète : 
    - [Préparation de l'UAL](https://nsi.janviercommelemois.fr/fichiers_circuits/circuits-pour-ual1.html)
    - [Préparation de l'UAL (suite)](https://nsi.janviercommelemois.fr/fichiers_circuits/circuits-pour-ual2.html)
    - (voici son cours sur l'[Unité Arithmétique et Logique](https://nsi.janviercommelemois.fr/fichiers_pdf/feuille-alu.pdf))
- de construire des circuits mémoires comme des verrous, des bascules, des registres et des compteurs : [Circuits mémoire](https://nsi.janviercommelemois.fr/fichiers_circuits/circuits-verrous.html)

---

**Références**

- Certaines idées proviennent d'exercices proposés par Romain Janvier : [Algèbre de Boole](https://nsi.janviercommelemois.fr/fichiers_pdf/feuille-algebre-boole.pdf), [Python - Algèbre de Boole](https://nsi.janviercommelemois.fr/fichiers_pdf/feuille-python-algebre-boole.pdf) et [Circuits logiques](https://nsi.janviercommelemois.fr/fichiers_pdf/feuille-circuits-logiques.pdf)
- Livre *Informatique et Sciences du Numérique, Terminale S*, de Gilles Dowek, éditions Eyrolles, disponible en [version PDF](https://wiki.inria.fr/wikis/sciencinfolycee/images/a/a7/Informatique_et_Sciences_du_Num%C3%A9rique_-_Sp%C3%A9cialit%C3%A9_ISN_en_Terminale_S._version_Python.pdf)
- Luc De Mey, pour l'idée du circuit *comparateur* : [https://courstechinfo.be/MathInfo/Comparateur.html](https://courstechinfo.be/MathInfo/Comparateur.html)
- Le simulateur logique en ligne [https://logic.modulo-info.ch/](https://logic.modulo-info.ch/) développé par Jean-Philippe Pellet.
- Le circuit *comparateur* de l'exercice 8 a été créé avec l'éditeur en ligne [https://www.circuit-diagram.org/editor/](https://www.circuit-diagram.org/editor/)

---
Germain Becker, Lycée Emmanuel Mounier, Angers.

![Licence Creative Commons](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)