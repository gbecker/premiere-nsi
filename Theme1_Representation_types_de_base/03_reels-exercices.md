Représentation binaire approximative des nombres réels - EXERCICES
==================================================================

# Exercice 1

1. Donnez l’écriture binaire du nombre réel 42,625.  
*On ne demande pas ici l’écriture sous la forme $s.m×2^n$ de la norme IEEE 754.*
2. Donnez l’écriture décimale du nombre binaire $(-1111011,00011)_2$.

# Exercice 2

> Capacité attendue !

1. Déterminez l’écriture binaire du nombre réel $0,1$. Son écriture binaire est-elle finie ou infinie et cyclique ?
2. Écrivez alors le nombre flottant correspondant sous la forme $s.m×2^n$, où $s$ est son signe, $m$ sa mantisse et $n$ son exposant.
3. En déduire le mot binaire représentant ce nombre flottant sur 32 bits (format simple précision).

# Exercice 3

> Capacité attendue !

Même questions avec le nombre réel $0,25$

# Exercice 4

> Capacité attendue !

Mêmes questions avec le nombre réel $\dfrac 1 3$. 

*Indication : il faut travailler avec les valeurs exactes donc vous devez garder les fractions dans les calculs des multiplications successives par 2*.

# Exercice 5

Pour chacune des questions suivantes, vous donnerez le résultat sous la forme $s.m×2^n$, où $s$ est son signe, $m$ sa mantisse et $n$ son exposant. Il faut considérer la représentation sur 64 bits (double précision).

1. Quel est le plus petit flottant strictement supérieur à `1.0` représentable en base 2 ?  
*Vous donnerez le résultat sous la forme $s.m×2^n$.*
2. Quel est le plus grand flottant strictement inférieur à `2.0` représentable en base 2 ?  
*Vous donnerez le résultat sous la forme $s.m×2^n$.*
3. Quel est le plus petit flottant strictement supérieur à $2^{53}=9007199254740992$ ?  
*Vous donnerez le résultat sous la forme $s.m×2^n$ puis sa valeur décimale.*  
Comment peut-on alors expliquer le résultat suivant ?
```python
>>> 9007199254740992.0 + 1.0 == 9007199254740992.0
True
```

# Exercice 6

1.	Déterminez le nombre flottant représenté par le mot de 32 bits (format simple précision) suivant :
```
0 00010111 11011100000000000000000
```

2. Même question avec le mot binaire :
```
1 01101100 00010001000000000000000
```

---
Germain Becker & Sébastien Point, Lycée Emmanuel Mounier, Angers
![Licence Creative Commons BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)


