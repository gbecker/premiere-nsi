Booléens : opérateurs et portes logiques - EXERCICES
====================================================

# Opérateurs booléens

## ✏️ Exercice 1 : Distributivité de *et* sur *ou*

Les opérateurs booléens vérifient la propriété de **distributivité** suivante, qui est vraie pour tous booléens *a*, *b* et *c* : 

$$\text{a et (b ou c) = (a et b) ou (a et c)}.$$

Recopier et compléter les tables de vérité suivantes pour démontrer cette égalité :

<div style="display:flex; flex-wrap:wrap; gap:2em; justify-content:center;">
    <div>
        <table>
            <thead>
                <tr>
                    <th>a</th>
                    <th>b</th>
                    <th>c</th>
                    <th>b ou c</th>
                    <th>a et (b ou c)</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>0</td>
                    <td>0</td>
                    <td>1</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>0</td>
                    <td>1</td>
                    <td>0</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>0</td>
                    <td>1</td>
                    <td>1</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>1</td>
                    <td>0</td>
                    <td>0</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>1</td>
                    <td>0</td>
                    <td>1</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>1</td>
                    <td>1</td>
                    <td>0</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>1</td>
                    <td>1</td>
                    <td>1</td>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
<!-- | a | b | c | b ou c | a et (b ou c) |
|---|---|---|--------|---------------|
| 0 | 0 | 0 |        |               |
| 0 | 0 | 1 |        |               |
| 0 | 1 | 0 |        |               |
| 0 | 1 | 1 |        |               |
| 1 | 0 | 0 |        |               |
| 1 | 0 | 1 |        |               |
| 1 | 1 | 0 |        |               |
| 1 | 1 | 1 |        |               | -->
    </div>
    <div>
        <table>
            <thead>
                <tr>
                    <th>a</th>
                    <th>b</th>
                    <th>c</th>
                    <th>a et b</th>
                    <th>a et c</th>
                    <th>(a et b) ou (a et c)</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>0</td>
                    <td>0</td>
                    <td>1</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>0</td>
                    <td>1</td>
                    <td>0</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>0</td>
                    <td>1</td>
                    <td>1</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>1</td>
                    <td>0</td>
                    <td>0</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>1</td>
                    <td>0</td>
                    <td>1</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>1</td>
                    <td>1</td>
                    <td>0</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>1</td>
                    <td>1</td>
                    <td>1</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
<!-- | a | b | c | a et b | a et c | (a et b) ou (a et c) |
|---|---|---|--------|--------|----------------------|
| 0 | 0 | 0 |        |        |                      |
| 0 | 0 | 1 |        |        |                      |
| 0 | 1 | 0 |        |        |                      |
| 0 | 1 | 1 |        |        |                      |
| 1 | 0 | 0 |        |        |                      |
| 1 | 0 | 1 |        |        |                      |
| 1 | 1 | 0 |        |        |                      |
| 1 | 1 | 1 |        |        |                      | -->
    </div>
</div>


## ✏️ Exercice 2 : Opérateur *xor* avec *et*, *ou* et *non*

**1.** Rappeler la table de vérité de l'opérateur ***xor***.

**2.** Construire, comme dans l'exercice précédent, la table de vérité de ***(a et non b) ou (non a et b)*** et vérifier que l'égalité suivante est vraie :

$$\text{a xor b = (a et non b) ou (non a et b)}.$$


## ✏️ Exercice 3 : Évaluation paresseuse

*Dans cet exercice, les expressions sont écrites dans le langage Python.*

**1.** Expliquer pourquoi l'expression `3 == 3 or x == y` est vraie pour toute valeur de `x` et de `y`.

**2.** Expliquer pourquoi l'expression `1 == 2 and x == y` est fausse pour toute valeur de `x` et de `y`.

<blockquote class="information" markdown="1">

Lorsque Python évalue une expression booléenne, il le fait de façon **paresseuse**. C'est-à-dire que si la partie gauche d'un `or` est vraie, il n'évalue pas la partie droite. De même, si la partie gauche d'un `and` est fausse, la partie droite n'est pas évaluée.

On peut le voir facilement : dans l'expression booléenne suivante, si la partie de droite était évaluée, Python aurait renvoyé une erreur de type `ZeroDivisionError` :

<pre class="language-python"><code>>>> x = 0
>>> 2 == 2 or 1/x > 0  # 2 == 2 est vrai donc 1/x > 0 n'est pas évalué
True</code></pre>

</blockquote>


## ✏️ Exercice 4 : Avec Python

**1.** Écrire en Python une fonction `xor(a, b)` qui renvoie la valeur de l'opération logique ***a xor b***, où *a* et *b* sont deux booléens représentés par `0` et `1`.

*Exemples :*

```python
>>> xor(1, 0)
1
>>> xor(1, 1)
0
```

**2.** L'opérateur ***non-et***, que l'on écrit ***nand*** en anglais (contraction de _**n**ot **and**_), est la succession des opérateurs ***et*** puis ***non***. On a donc pour tout booléen *a* et *b* : 

$$\text{a nand b = non (a et b)}$$

**2a.** Donner la table de vérité de l'opérateur ***nand***.

**2b.** Écrire une fonction Python `nand(a, b)` qui renvoie la valeur de ***a nand b***, où *a* et *b* sont deux booléens représentés par `0` et `1`.

*Exemple* :

```python
>>> nand(1, 0)
1
```


# Portes et circuits logiques

## ✏️ Exercice 5 : Porte NAND

Une porte NAND permet de réaliser l'opération logique ***nand*** (ou ***non-et*** en français). On rappelle (voir exercice précédent) que cet opérateur est défini de la manière suivante :

$$\text{a nand b = non (a et b)}$$

**1.** Construire avec le simulateur une porte NAND (ou NON-ET) en utilisant une porte NON et une porte ET.  *Le circuit est à recopier sur votre feuille.*

<div class="logic-editor centre" style="height: 420px;">
  <logic-editor mode="design" showonly="and not in out">
    <script type="application/json">
      { // JSON5
        v: 6
      }
    </script>
  </logic-editor>
</div>

**2**. Vérifier, avec le simulateur, que le circuit est correct en comparant avec la table de vérité construite dans l'exercice précédent.


## ✏️ Exercice 6 : Loi de De Morgan

L'objectif de l'exercice est de démontrer la loi de [De Morgan](https://fr.wikipedia.org/wiki/Auguste_De_Morgan) suivante : 

>Pour tous booléens $a$ et $b$, on a :  $\text{non (a et b) = (non a) ou (non b)}$.


Le circuit ci-dessous correspond à l'expression booléenne ***non (a et b)*** (celle de gauche dans l'égalité) :

<div class="logic-editor centre" style="height: 140px;">
  <logic-editor mode="tryout">
    <script type="application/json">
      { // JSON5
        v: 6,
        opts: {showGateTypes: true},
        components: {
          in0: {type: 'in', pos: [70, 45], id: 0, name: 'a'},
          in1: {type: 'in', pos: [70, 95], id: 1, name: 'b'},
          and0: {type: 'and', pos: [190, 70], in: [2, 3], out: 4},
          not0: {type: 'not', pos: [270, 70], in: 6, out: 7},
          out0: {type: 'out', pos: [340, 70], id: 8},
        },
        wires: [[0, 2], [1, 3], [4, 6], [7, 8]]
      }
    </script>
  </logic-editor>
</div>

**1.** Construire le circuit correspondant à l'expression booléenne ***(non a) ou (non b)*** (celle de droite dans l'égalité). *Le circuit est à recopier sur votre feuille.*

<div class="logic-editor centre" style="height: 420px;">
  <logic-editor mode="design" showonly="or not in out">
    <script type="application/json">
      { // JSON5
        v: 6
      }
    </script>
  </logic-editor>
</div>

**2.** Vérifier, en utilisant le simulateur, que les deux circuits sont équivalents, c'est-à-dire qu'ils ont la même table de vérité.


## ✏️ Exercice 7 : Porte XOR avec ET, OU  et NON

**1.** Construire le circuit correspondant à l'expression ***(a et non b) ou (non a et b)***. *Le circuit est à recopier sur votre feuille.*

<div class="logic-editor centre" style="height: 500px;">
  <logic-editor mode="design" showonly="or not in out and">
    <script type="application/json">
      { // JSON5
        v: 6
      }
    </script>
  </logic-editor>
</div>

**2.** Vérifier, en utilisant le simulateur, que ce circuit est équivalent à une porte XOR.


## ✏️ Exercice 8 : Comparateur

Dans cet exercice on considère le circuit suivant, appelé **comparateur**, qui permet de comparer des mots binaires (a1, a0) et (b1, b0) de longueur 2. Le bit de sortie :
- vaut 1 si  (a1, a0) = (b1, b0) ;
- vaut 0 sinon.

<img class="centre image-responsive" alt="circuit comparateur" src="data/comparateur.svg" width="600">

**1.** Construire ce circuit avec le simulateur.

<div class="logic-editor centre" style="height: 550px;">
  <logic-editor mode="design" showonly="xor not and xnor in out">
    <script type="application/json">
      { // JSON5
        v: 6
      }
    </script>
  </logic-editor>
</div>

**2.** Recopier et compléter (en utilisant directement le simulateur) la table de vérité de ce circuit.

| a1 | a0 | b1 | b0 | sortie |
|----|----|----|----|--------|
| 0  | 0  | 0  | 0  |        |
| 0  | 0  | 0  | 1  |        |
| 0  | 0  | 1  | 0  |        |
| 0  | 0  | 1  | 1  |        |
| 0  | 1  | 0  | 0  |        |
| 0  | 1  | 0  | 1  |        |
| 0  | 1  | 1  | 0  |        |
| 0  | 1  | 1  | 1  |        |
| 1  | 0  | 0  | 0  |        |
| 1  | 0  | 0  | 1  |        |
| 1  | 0  | 1  | 0  |        |
| 1  | 0  | 1  | 1  |        |
| 1  | 1  | 0  | 0  |        |
| 1  | 1  | 0  | 1  |        |
| 1  | 1  | 1  | 0  |        |
| 1  | 1  | 1  | 1  |        |

**3.** Expliquer, en s'appuyant sur la table de vérité, pourquoi ce circuit permet bien de comparer deux mots binaires de longueur 2.

>On peut comparer des mots de longueur supérieure à deux en ajoutant d'autres portes NON XOR (notée aussi *NON OU exclusif* ou encore *XNOR*).

**4.** Donner une expression booléenne écrite en Python qui nécessiterait d'utiliser un tel circuit logique pour être évaluée.


## ✏️ Exercice 9 : Exploration (optionnel)

En utilisant des portes NON, ET, OU, dessiner un circuit logique qui a 4 entrées A, B, C et D et dont la sortie S vaut 1 si et seulement si au moins une des entrées est non nulle.

<div class="logic-editor centre" style="height: 350px">
  <logic-editor mode="design" showonly="not and or">
    <script type="application/json">
      { // JSON5
        v: 6,
        components: {
          in0: {type: 'in', pos: [80, 75], id: 0, name: 'A'},
          in1: {type: 'in', pos: [80, 120], id: 1, name: 'B'},
          in2: {type: 'in', pos: [80, 165], id: 2, name: 'C'},
          in3: {type: 'in', pos: [80, 210], id: 3, name: 'D'},
          out0: {type: 'out', pos: [630, 145], id: 4, name: 'S'},
        }
      }
    </script>
  </logic-editor>
</div>


# Exercices d'approfondissement (optionnel)

Pour celles et ceux qui souhaiteraient aller approfondir les notions de ce chapitre, vous pouvez chercher les exercices suivants, élaborés par Romain Janvier, qui propose :

- de construire pas à pas une UAL (unité arithmétique et logique) complète : 
    - [Préparation de l'UAL](https://nsi.janviercommelemois.fr/fichiers_circuits/circuits-pour-ual1.html)
    - [Préparation de l'UAL (suite)](https://nsi.janviercommelemois.fr/fichiers_circuits/circuits-pour-ual2.html)
    - (voici son cours sur l'[Unité Arithmétique et Logique](https://nsi.janviercommelemois.fr/fichiers_pdf/feuille-alu.pdf))
- de construire des circuits mémoires comme des verrous, des bascules, des registres et des compteurs : [Circuits mémoire](https://nsi.janviercommelemois.fr/fichiers_circuits/circuits-verrous.html)

---

**Références**

- Certaines idées proviennent d'exercices proposés par Romain Janvier : [Algèbre de Boole](https://nsi.janviercommelemois.fr/fichiers_pdf/feuille-algebre-boole.pdf), [Python - Algèbre de Boole](https://nsi.janviercommelemois.fr/fichiers_pdf/feuille-python-algebre-boole.pdf) et [Circuits logiques](https://nsi.janviercommelemois.fr/fichiers_pdf/feuille-circuits-logiques.pdf)
- Livre *Informatique et Sciences du Numérique, Terminale S*, de Gilles Dowek, éditions Eyrolles, disponible en [version PDF](https://wiki.inria.fr/wikis/sciencinfolycee/images/a/a7/Informatique_et_Sciences_du_Num%C3%A9rique_-_Sp%C3%A9cialit%C3%A9_ISN_en_Terminale_S._version_Python.pdf)
- Luc De Mey, pour l'idée du circuit *comparateur* : [https://courstechinfo.be/MathInfo/Comparateur.html](https://courstechinfo.be/MathInfo/Comparateur.html)
- Le simulateur logique en ligne [https://logic.modulo-info.ch/](https://logic.modulo-info.ch/) développé par Jean-Philippe Pellet.
- Le circuit *comparateur* de l'exercice 8 a été créé avec l'éditeur en ligne [https://www.circuit-diagram.org/editor/](https://www.circuit-diagram.org/editor/)

---
Germain Becker, Lycée Emmanuel Mounier, Angers.

![Licence Creative Commons](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)